﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using AeroportSimulator.Client.Components;
using AeroportSimulator.Server;
using AeroportSimulator.Server.Api;
using AeroportSimulator.Server.Api.Event;
using AeroportSimulator.Server.Common;

namespace AeroportSimulator.Client.Forms
{
    public partial class BankruptcyForm : Form
    {
        /// <summary>
        /// Текущий игрок
        /// </summary>
        private IPlayer _currentPlayer;

        /// <summary>
        /// Трэйдер, которому продаем самолеты
        /// </summary>
        private IAirplaneTraider _traider;

        /// <summary>
        /// Словарь самолет-контрол (с инфой)
        /// </summary>
        private Dictionary<IAirplane, AirplaneComponent> _playerBindedAirMap; 

        public BankruptcyForm()
        {
            InitializeComponent();
            _playerBindedAirMap = new Dictionary<IAirplane, AirplaneComponent>();

            _traider = AeroportServer.Instance.Traider;
            _currentPlayer = AeroportServer.Instance.Player;
            InitPanel();
            lbBalance.Text += $"{_currentPlayer.Money}";
            AeroportServer.Instance.RegisterEventHandler(this, SynchronizationContext.Current);
        }

        private void InitPanel()
        {
            foreach (var airplane in _currentPlayer.Airplanes)
            {
                var component = new AirplaneComponent(airplane, true);
                _playerBindedAirMap.Add(airplane, component);
                panelBuyedAirplanes.Controls.Add(component);
            }
        }

        private void CloseBankruptcyForm(object sender, FormClosedEventArgs e)
        {
            AeroportServer.Instance.UnregisterEventHandler(this);
        }

        private void btSellAirplane_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < _playerBindedAirMap.Count(x => x.Value.IsSelected); i++)
            {
                var airControl = _playerBindedAirMap.Values.First(x => x.IsSelected);
                var airplane = _playerBindedAirMap.First(x => x.Value == airControl).Key;
                if (!_traider.SellAirplane(_currentPlayer, airplane))
                {
                    MessageBox.Show("Неудалось выполнить данную операцию!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        /// <summary>
        /// Продажа самолета
        /// </summary>
        /// <param name="e"></param>
        [EventHandlerAttribute]
        public void PlayerSellFlight(PlayerAirplaneRemovingEvent e)
        {
            var component = _playerBindedAirMap[e.Airplane];
            panelBuyedAirplanes.Controls.Remove(component);
            _playerBindedAirMap.Remove(e.Airplane);
        }

        /// <summary>
        /// Изменение контракта лизинга для n-го самолета
        /// </summary>
        /// <param name="e"></param>
        [EventHandlerAttribute]
        public void ChangeLeaseContract(TraiderLeasingContractPaymentEvent e)
        {
            var airControl = _playerBindedAirMap.First(x => x.Key == e.Contract.Airplane).Value;
            airControl.numTime.Value = e.Contract.ResidualTime / TimeHelper.SECONDS_IN_MONTH;
        }

        /// <summary>
        /// Аналог того, что выше только для аренды
        /// </summary>
        /// <param name="e"></param>
        [EventHandlerAttribute]
        public void ChangeRentContract(TraiderRentContractPaymentEvent e)
        {
            var airControl = _playerBindedAirMap.First(x => x.Key == e.Contract.Airplane).Value;
            airControl.numTime.Value = e.Contract.ResidualTime / TimeHelper.SECONDS_IN_MONTH;
        }


        /// <summary>
        /// Смена города у самолета
        /// </summary>
        [EventHandlerAttribute]
        public void ChangeAirCity(AirplaneMoveEvent e)
        {
            var airControl = _playerBindedAirMap.First(x => x.Key == e.Airplane).Value;
            airControl.lbCurrentCity.Text = $"Город: {e.Airplane.CurrentCity}";
        }

        /// <summary>
        /// Смена баланса игрока
        /// </summary>
        /// <param name="e"></param>
        [EventHandlerAttribute]
        public void PlayerBalanceChange(PlayerMoneyChangeEvent e)
        {
            lbBalance.Text = $"Ваш баланс: {e.Player.Money}";
        }
    }
}
