﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using AeroportSimulator.Client.Components;
using AeroportSimulator.Server;
using AeroportSimulator.Server.Api;
using AeroportSimulator.Server.Api.Event;
using AeroportSimulator.Server.Common;
using AeroportSimulator.Server.Common.Factory;
using AeroportSimulator.Server.Common.Lib;

namespace AeroportSimulator.Client.Forms
{
    public partial class AerportSimulatorForm : Form
    {
        /// <summary>
        /// Класс для описания параметров столбцов в таблице
        /// </summary>
        class TableColumnData
        {
            public string Title { get; set; }
            public Type Type { get; set; }
            public int Width { get; set; }

            public TableColumnData(string title, Type type, int width)
            {
                Title = title;
                Type = type;
                Width = width;
            }
        }

        /// <summary>
        /// сервер игры
        /// </summary>
        private ISimulationServer _server;

        /// <summary>
        /// текущий игрок
        /// </summary>
        private IPlayer _currentPlayer;

        /// <summary>
        /// словарь рейс-контрол (содержит инфу о рейсе)
        /// </summary>
        private Dictionary<IFlight, FlightComponent> _fligtMap;

        /// <summary>
        /// словарь рейс-строка в гриде (содержащая нужную инфу)
        /// </summary>
        private Dictionary<IFlight, DataGridViewRow> _flightBindedMap;

        /// <summary>
        /// Осуществляется ли игра в данный момент
        /// </summary>
        private bool _isPlaying;

        public AerportSimulatorForm()
        {
            InitializeComponent();
            InitGame();
            InitGridView();
        }

        /// <summary>
        /// Инициализация сервера(ну тоесть всей нечисти, что творится в игре)
        /// </summary>
        private void InitGame()
        {
            Trace.TraceInformation($"{DateTime.Now}: Началась игра ");
            _fligtMap = new Dictionary<IFlight, FlightComponent>();
            _flightBindedMap = new Dictionary<IFlight, DataGridViewRow>();
            _isPlaying = true;

            _server = AeroportServerFactory.Create(new ServerParams());
            _currentPlayer = _server.Player;
            lbBalance.Text = $"Ваш текущий баланс: {_currentPlayer.Money}$";
            lbHome.Text = $"Ваш город: {_server.Player.Home}";
            Trace.TraceInformation($"{DateTime.Now}: Деньги игрока {_currentPlayer.Money}");
            dtmData.Value = _server.CurrentDate();
            _server.RegisterEventHandler(this, SynchronizationContext.Current);
            _server.Start();
        }

        #region User events

        /// <summary>
        /// Игрок купил/арендовал/лизинг самолет (надо обновить контролы рейсов)
        /// </summary>
        /// <param name="e"></param>
        [EventHandlerAttribute]
        public void PlayerAddAirplane(PlayerAirplaneAddingEvent e)
        {
            foreach (var flight in _fligtMap.Values)
            {
                flight.UpdateData();
            }
        }

        /// <summary>
        /// Игрок убрал из продал/вернул самолет (надо обновить контролы рейсов)
        /// </summary>
        /// <param name="e"></param>
        [EventHandlerAttribute]
        public void PlayerRemoveAirplane(PlayerAirplaneRemovingEvent e)
        {
            foreach (var flight in _fligtMap.Values)
            {
                flight.UpdateData();
            }
        }

        /// <summary>
        /// Пишем баланс в лэйбл для отображения
        /// </summary>
        /// <param name="e"></param>
        [EventHandlerAttribute]
        public void PlayerBalanceChange(PlayerMoneyChangeEvent e)
        {
            lbBalance.Text = $"Ваш текущий баланс: {e.Player.Money}$";
        }

        #endregion

        #region Flight events

        /// <summary>
        /// Изменение прогрессбара у рейса
        /// </summary>
        [EventHandlerAttribute]
        public void FlightProgressChange(FlightProgressEvent e)
        {
            DataGridViewRow dataRow;
            if (_flightBindedMap.TryGetValue(e.Flight, out dataRow))
            {
                dataRow.Cells[7].Value = e.Progress * 100;
            }
        }

        /// <summary>
        /// Генерация рейса
        /// </summary>
        /// <param name="e"></param>
        [EventHandlerAttribute]
        public void OnNewFlight(WorldAddFlightEvent e)
        {
            var flight = e.Flight;
            var component = new FlightComponent(flight, _currentPlayer);
            _fligtMap.Add(e.Flight, component);
            panelFlights.Controls.Add(component);
            Trace.TraceInformation($"{DateTime.Now}: добавлен рейс {flight.Uuid}");
        }

        /// <summary>
        /// Удаление рейса
        /// </summary>
        /// <param name="e"></param>
        [EventHandlerAttribute]
        public void OnRemoveFlight(WorldRemoveFlightEvent e)
        {
            FlightComponent c;
            if (_fligtMap.TryGetValue(e.Flight, out c))
            {
                panelFlights.Controls.Remove(c);
                _fligtMap.Remove(e.Flight);
            }
            Trace.TraceInformation($"{DateTime.Now}: удален рейс {e.Flight.Uuid}");
        }

        /// <summary>
        /// Заполнить строку инфой
        /// </summary>
        /// <param name="f"></param>
        /// <param name="dataRow"></param>
        private void FillRow(IFlight f, DataGridViewRow dataRow)
        {
            dataRow.Cells[0].Value = f.Uuid;
            dataRow.Cells[1].Value = f.Airplane.Type.Name;
            dataRow.Cells[2].Value = f.SrcCity;
            dataRow.Cells[3].Value = f.DestCity;
            dataRow.Cells[4].Value = AeroportServer.Instance.TimeToDateTime(f.DepartureTime);
            dataRow.Cells[5].Value = AeroportServer.Instance.TimeToDateTime(f.ArrivalTime);
            dataRow.Cells[6].Value = $"{f.State}";
        }

        /// <summary>
        /// Удалить строку из таблицы есть есть
        /// </summary>
        /// <param name="f"></param>
        private void RemoveFlightRow(IFlight f)
        {
            DataGridViewRow row;
            if (_flightBindedMap.TryGetValue(f, out row))
            {
                dgvAeorport.Rows.Remove(row);
                _flightBindedMap.Remove(f);
            }
        }

        /// <summary>
        /// Добавление рейса в список обслуживаемых
        /// </summary>
        /// <param name="e"></param>
        [EventHandlerAttribute]
        public void OnBindFlight(BindFlightEvent e)
        {
            var dataRow = new DataGridViewRow();
            dataRow.CreateCells(dgvAeorport);
            FillRow(e.Flight, dataRow);
            _flightBindedMap.Add(e.Flight, dataRow);
            Trace.TraceInformation($"{DateTime.Now}: игрок добавл рейс {e.Flight.Uuid} в список своих рейсов");

            //удаление рейса из панельки рейсов
            if (_fligtMap.ContainsKey(e.Flight))
            {
                panelFlights.Controls.Remove(_fligtMap[e.Flight]);
                _fligtMap.Remove(e.Flight);
            }
            
            //старт рейса если возможен иначе мессадж смените самолет, иначе всрали бабки, отказаться нельзя
            CheckAllRows();

            if (e.Flight.CanStart())
            {
                e.Flight.Start();
            }


            dataRow.ContextMenuStrip = new ContextMenuStrip();
            dataRow.ContextMenuStrip.AutoSize = true;
            dataRow.ContextMenuStrip.Items.Add("Запустить рейс").Click += (s, ev) =>
            {
                if (e.Flight.CanStart())
                {
                    e.Flight.Start();
                    CheckAllRows();
                }
            };
            dataRow.ContextMenuStrip.Items.Add("Обслужить самолет").Click += (s, ev) =>
            {
                AeroportServer.Instance.ServiceAirplane(e.Flight.Airplane);
                CheckAllRows();
            };
            dataRow.ContextMenuStrip.Items.Add("Сменить самолет").Click += (s, ev) =>
            {
                
                var newAir = SelectBox.Show(AeroportServer.Instance.Player.Airplanes);
                if (newAir != null)
                {
                    if (e.Flight.Airplane.UsedInFlight != null)
                    {
                        MessageBox.Show("Не удалось сменить самолет. Он уже находится в полете", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    if (e.Flight.ReplaceAirplane(newAir))
                    {
                        FillRow(e.Flight, dataRow);
                        CheckAllRows();
                    }
                    else
                    {
                        MessageBox.Show("Не удалось сменить самолет", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            };
            dataRow.ContextMenuStrip.Items.Add("Проверить условия").Click += (s, ev) => CheckAllRows();
            dgvAeorport.Rows.Add(dataRow);
        }

        private void CheckAllRows()
        {
            foreach (var entry in _flightBindedMap)
            {
                var flight = entry.Key;
                var row = entry.Value;
                row.ErrorText = !flight.CanStart() ? $"Несоблюдены условия:\n\t{String.Join("\n\t", flight.CheckConditions())}" : null;
            }
        }

        /// <summary>
        /// Старт рейса
        /// </summary>
        /// <param name="e"></param>
        [EventHandlerAttribute]
        public void OnStartFlight(StartFlightEvent e)
        {
            Trace.TraceInformation($"{DateTime.Now}: рейс {e.Flight.Uuid} стартовал");
        }

        /// <summary>
        /// Окончание рейса
        /// </summary>
        /// <param name="e"></param>
        [EventHandlerAttribute]
        public void OnFinishFlight(FinishFlightEvent e)
        {
            RemoveFlightRow(e.Flight);
            Trace.TraceInformation($"{DateTime.Now}: рейс {e.Flight.Uuid} финишировал");
        }

        /// <summary>
        /// Изменение статуса рейса
        /// </summary>
        /// <param name="e"></param>
        [EventHandlerAttribute]
        public void OnStatusFlight(FlightStatusChangeEvent e)
        {
            DataGridViewRow row;
            if (_flightBindedMap.TryGetValue(e.Flight, out row))
            {
                FillRow(e.Flight, row);
            }
            Trace.TraceInformation($"{DateTime.Now}: рейс {e.Flight.Uuid} изменил статус на " + e.State);
        }

        /// <summary>
        /// Просрок рейса
        /// </summary>
        /// <param name="e"></param>
        [EventHandlerAttribute]
        public void OnOverdueFlight(OverdueFlightEvent e)
        {
            RemoveFlightRow(e.Flight);
            Trace.TraceInformation($"{DateTime.Now}: рейс {e.Flight.Uuid} просрочился");
        }

        [EventHandlerAttribute]
        public void OnServerLogMessage(ServerTraceMessageEvent e)
        {
            tbLog.AppendText(e.Message);
            tbLog.AppendText(Environment.NewLine);
        }

        [EventHandlerAttribute]
        public void OnLeasingRedeemRequest(LeasingRedeemRequestEvent e)
        {
            var result = MessageBox.Show("Время лизинга самолета: " + e.Airplane + " подошло к концу.\n" +
                "Хотите выкупить самолет по остаточной стоимости: " + e.ResidualPrice + "$?", "Сообщение", 
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            e.Agree = result == DialogResult.Yes;
            e.Finish();
        }

        [EventHandlerAttribute]
        public void OnBankruptProcedure(BankruptProcedureBeginEvent e)
        {
            new BankruptcyForm().ShowDialog();
            e.Finish();
        }

        //todo
        //[EventHandlerAttribute]
        //public void FlightAirplaneStateChange(Flight)
        #endregion

        #region Server events

        /// <summary>
        /// Тик жизни сервера
        /// </summary>
        /// <param name="e"></param>
        [EventHandlerAttribute]
        public void OnServerTick(ServerTickEvent e)
        {
            dtmData.Value = _server.CurrentDate();
        }

        /// <summary>
        /// Трассировка серверных событий
        /// </summary>
        /// <param name="e"></param>
        [EventHandlerAttribute]
        public void TraceToLog(ServerTraceMessageEvent e)
        {
            Trace.TraceInformation(e.Message);
        }

        /// <summary>
        /// Конец игры
        /// </summary>
        /// <param name="e"></param>
        [EventHandlerAttribute]
        public void GameOver(GameOverEvent e)
        {
            Trace.TraceError($"Игрок проиграл по причине: {e.Reason}");
            MessageBox.Show($"К сожалению вы проиграли из-за того, что {e.Reason}", $"Конец игры");
        }
        #endregion

        #region Initialize Form

        readonly List<TableColumnData> _listColumns = new List<TableColumnData>()
        {
            new TableColumnData("Рейс", typeof(DataGridViewTextBoxColumn), 100),
            new TableColumnData("Самолет", typeof(DataGridViewTextBoxColumn), 100),
            new TableColumnData("Откуда", typeof(DataGridViewTextBoxColumn), 100),
            new TableColumnData("Куда", typeof(DataGridViewTextBoxColumn), 100),
            new TableColumnData("Дата вылета", typeof(DataGridViewCalendarColumn), 110),
            new TableColumnData("Дата посадки", typeof(DataGridViewCalendarColumn), 110),
            new TableColumnData("Статус", typeof(DataGridViewTextBoxColumn), 100),
            new TableColumnData("Прогресс", typeof(DataGridViewProgressColumn), 100)
        };

        /// <summary>
        /// Инициализация грида на основе словаря выше
        /// </summary>
        private void InitGridView()
        {
            foreach (var c in _listColumns)
            {
                var column = Activator.CreateInstance(c.Type) as DataGridViewColumn;
                if (column == null) continue;
                column.HeaderText = c.Title;
                column.ReadOnly = true;
                column.Width = c.Width;
                //смена цвета прогресс бара
                if (column is DataGridViewProgressColumn)
                {
                    var progressColumn = column as DataGridViewProgressColumn;
                    progressColumn.ProgressBarColor = Color.Green;
                    column = progressColumn;
                }
                dgvAeorport.Columns.Add(column);
            }
        }

        #endregion

        #region Form events

        /// <summary>
        /// Открытие магазина (синглтон, поток неперекрывает)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuItemShop_Click(object sender, EventArgs e)
        {
            Trace.TraceInformation($"{DateTime.Now}: игрок зашел в магазин");
            AirShopForm airShopForm = AirShopForm.Instance;
            airShopForm.Show();
        }

        /// <summary>
        /// Открытие формы кастомного рейса
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btEmptyFlight_Click(object sender, EventArgs e)
        {
            Trace.TraceInformation($"{DateTime.Now}: игрок открыл форму создания рейса");
            CustomFlightForm customFlightForm = CustomFlightForm.Instance;
            customFlightForm.Show();
        }

        /// <summary>
        /// Регулировка скорости
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void numHaste_ValueChanged(object sender, EventArgs e)
        {
            _server.SetTickInterval((long)Math.Exp(numHaste.Value));
        }

        /// <summary>
        /// Удаление рейса из пользования игрока
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlayerDeleteFlight(object sender, DataGridViewRowCancelEventArgs e)
        {
            var flight = _flightBindedMap.FirstOrDefault(x => x.Value == e.Row).Key;
            if (DialogResult.Yes == MessageBox.Show($"Вы уверены, что хотите удалить рейс {flight.Uuid}?",
                $"Информация", MessageBoxButtons.YesNo))
            {
                Trace.TraceInformation($"Игрок удалил рейс {flight.Uuid} из своего пользования");
                flight.UnBind();
                _flightBindedMap.Remove(flight);
            }
        }

        /// <summary>
        /// Остановить/продолжить игру
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btStopResume_Click(object sender, EventArgs e)
        {
            _isPlaying = !_isPlaying;
            var button = sender as Button;
            EnableRecurse(_isPlaying, this);
            button.Text = _isPlaying ? "Пауза" : "Продолжить";
            button.Enabled = true;

            if (!_isPlaying)
            {
                _server.Pause();
            }
            else
            {
                _server.Resume();
            }
        }

        public void EnableRecurse(bool enabled, Control c = null)
        {
            if (c != null)
            {
                foreach (var cc in c.Controls)
                {
                    var con = cc as Control;
                    EnableRecurse(enabled, con);
                    if (con.GetType() == typeof(GroupBox)) continue;
                    if (con.GetType() == typeof(Panel)) continue;
                    con.Enabled = enabled;
                }
            }
        }

        /// <summary>
        /// Закрытие формы (конец игры)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EndGame(object sender, FormClosedEventArgs e)
        {
            _server.Stop();
        }


        #endregion

        private void mapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new MapForm().Show();
        }
    }
}