﻿namespace AeroportSimulator.Client.Forms
{
    partial class BankruptcyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelBuyedAirplanes = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbBalance = new System.Windows.Forms.Label();
            this.btSellAirplane = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelBuyedAirplanes
            // 
            this.panelBuyedAirplanes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelBuyedAirplanes.AutoScroll = true;
            this.panelBuyedAirplanes.Location = new System.Drawing.Point(6, 6);
            this.panelBuyedAirplanes.Name = "panelBuyedAirplanes";
            this.panelBuyedAirplanes.Size = new System.Drawing.Size(228, 172);
            this.panelBuyedAirplanes.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lbBalance);
            this.panel1.Controls.Add(this.btSellAirplane);
            this.panel1.Controls.Add(this.panelBuyedAirplanes);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(3);
            this.panel1.Size = new System.Drawing.Size(240, 226);
            this.panel1.TabIndex = 4;
            // 
            // lbBalance
            // 
            this.lbBalance.AutoSize = true;
            this.lbBalance.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lbBalance.Location = new System.Drawing.Point(3, 181);
            this.lbBalance.Margin = new System.Windows.Forms.Padding(3);
            this.lbBalance.Name = "lbBalance";
            this.lbBalance.Padding = new System.Windows.Forms.Padding(3);
            this.lbBalance.Size = new System.Drawing.Size(79, 19);
            this.lbBalance.TabIndex = 5;
            this.lbBalance.Text = "Ваш баланс: ";
            // 
            // btSellAirplane
            // 
            this.btSellAirplane.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btSellAirplane.Location = new System.Drawing.Point(3, 200);
            this.btSellAirplane.Margin = new System.Windows.Forms.Padding(6);
            this.btSellAirplane.Name = "btSellAirplane";
            this.btSellAirplane.Size = new System.Drawing.Size(234, 23);
            this.btSellAirplane.TabIndex = 4;
            this.btSellAirplane.Text = "Продать";
            this.btSellAirplane.UseVisualStyleBackColor = true;
            // 
            // BankruptcyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(240, 226);
            this.Controls.Add(this.panel1);
            this.Name = "BankruptcyForm";
            this.Text = "BankruptcyForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CloseBankruptcyForm);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.FlowLayoutPanel panelBuyedAirplanes;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbBalance;
        private System.Windows.Forms.Button btSellAirplane;
    }
}