﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using AeroportSimulator.Client.Components;
using AeroportSimulator.Server;
using AeroportSimulator.Server.Api;
using AeroportSimulator.Server.Api.Event;
using AeroportSimulator.Server.Common;

namespace AeroportSimulator.Client.Forms
{
    public partial class AirShopForm : Form
    {
        /// <summary>
        /// Синглтон
        /// </summary>
        private static AirShopForm _instance;

        /// <summary>
        /// Словарь самолет-контрол (аренда, лизинг, продажа)
        /// </summary>
        private Dictionary<object, AirplaneComponent> _airplaneMap;

        /// <summary>
        /// Словарь самолет игрока-контрол
        /// </summary>
        private Dictionary<IAirplane, AirplaneComponent> _airplaneBindedMap; 

        /// <summary>
        /// Текущий игрок
        /// </summary>
        private IPlayer _currentPlayer;

        /// <summary>
        /// Трэйдер
        /// </summary>
        private IAirplaneTraider _traider;

        /// <summary>
        /// Синглтон
        /// </summary>
        public static AirShopForm Instance
        {
            get
            {
                if (_instance == null || _instance.IsDisposed)
                    _instance = new AirShopForm();
                return _instance;
            }
        }

        public AirShopForm()
        {
            InitializeComponent();
            _airplaneMap = new Dictionary<object, AirplaneComponent>();
            _airplaneBindedMap = new Dictionary<IAirplane, AirplaneComponent>();

            AeroportServer.Instance.RegisterEventHandler(this, SynchronizationContext.Current);
            _currentPlayer = AeroportServer.Instance.Player;
            lbBalance.Text = $"Ваш текущий баланс: {_currentPlayer.Money}$";
            _traider = AeroportServer.Instance.Traider;
            DisplayTraiderShowcase();
            ShowUserAirplanes();
        }

        public void DisplayTraiderShowcase()
        {
            traiderPanel.Controls.Clear();
            _airplaneMap.Clear();
            foreach (var term in _traider.LeasingTerms().Union(_traider.RentTerms()))
            {
                var component = new AirplaneComponent(term, false);
                traiderPanel.Controls.Add(component);
                _airplaneMap.Add(term, component);
            }
            foreach (var air in _traider.AirplaneShowcase())
            {
                var component = new AirplaneComponent(air, false);
                traiderPanel.Controls.Add(component);
                _airplaneMap.Add(air, component);
            }
        }

        /// <summary>
        /// Обновление ассортимента магазина
        /// </summary>
        /// <param name="e"></param>
        [EventHandlerAttribute]
        public void UpdateShopEvent(TraiderShowcaseUpdateEvent e)
        {
            DisplayTraiderShowcase();
            Trace.TraceInformation($"{DateTime.Now}: Ассортимен магазина обновился");
        }

        [EventHandlerAttribute]
        public void PlayerBalanceChange(PlayerMoneyChangeEvent e)
        {
            lbBalance.Text = $"Ваш текущий баланс: {e.Player.Money}$";
        }

        /// <summary>
        /// Добавление (покупка, аренда, лизинг) самолета
        /// </summary>
        /// <param name="e"></param>
        [EventHandlerAttribute]
        public void PlayerAddFlight(PlayerAirplaneAddingEvent e)
        {
            var component = new AirplaneComponent(e.Airplane, true);
            _airplaneBindedMap.Add(e.Airplane, component);
            playerPanel.Controls.Add(component);
        }

        /// <summary>
        /// Продажа/ушел из аренды/лизинга самолет
        /// </summary>
        /// <param name="e"></param>
        [EventHandlerAttribute]
        public void PlayerSellFlight(PlayerAirplaneRemovingEvent e)
        {
            var component = _airplaneBindedMap[e.Airplane];
            playerPanel.Controls.Remove(component);
            _airplaneBindedMap.Remove(e.Airplane);
        }

        /// <summary>
        /// Отображение самолетов пользователя
        /// </summary>
        private void ShowUserAirplanes()
        {
            foreach (var air in _currentPlayer.Airplanes)
            {
                var component = new AirplaneComponent(air, true);
                _airplaneBindedMap.Add(air, component);
                playerPanel.Controls.Add(component);
            }
        }

        /// <summary>
        /// Покупка, аренда, лизинг выбранного самолета
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            foreach (var airControl in _airplaneMap.Values.Where(x => x.IsSelected))
            {
                var airplane = _airplaneMap.FirstOrDefault(x => x.Value == airControl).Key;
                if (airplane.GetType() == typeof (LeasingTerms))
                {
                    var air = airplane as LeasingTerms;
                    if (_traider.LeaseAirplane(_currentPlayer, air, airControl.Time) == null)
                    {
                        MessageBox.Show("Недостаточно денег!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                if (airplane.GetType() == typeof (RentTerms))
                {
                    var air = airplane as RentTerms;
                    if (_traider.RentAirplane(_currentPlayer, air, airControl.Time) == null)
                    {
                        MessageBox.Show("Недостаточно денег!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                if (airplane.GetType() == typeof (AirplaneType))
                {
                    var air = airplane as AirplaneType;
                    if (_traider.BuyAirplane(_currentPlayer, air) == null)
                    {
                        MessageBox.Show("Недостаточно денег!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            //unselect
            foreach (var airplane in _airplaneMap.Values)
            {
                airplane.IsSelected = false;
                airplane.BackColor = SystemColors.Control;
            }
        }

        /// <summary>
        /// Удаление выбранных самолетов игрока
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            var selectedPlayerAirplanes = _airplaneBindedMap.Where(x => x.Value.IsSelected).ToList();
            for (var i = 0; i < selectedPlayerAirplanes.Count; i++)
            {
                var airplane = selectedPlayerAirplanes[i].Key;
                if (!_traider.SellAirplane(_currentPlayer, airplane))
                {
                    MessageBox.Show("Неудалось выполнить данную операцию!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        /// <summary>
        /// Изменение контракта лизинга для n-го самолета
        /// </summary>
        /// <param name="e"></param>
        [EventHandlerAttribute]
        public void ChangeLeaseContract(TraiderLeasingContractPaymentEvent e)
        {
            var airControl = _airplaneBindedMap.First(x => x.Key == e.Contract.Airplane).Value;
            airControl.numTime.Value = e.Contract.ResidualTime/TimeHelper.SECONDS_IN_MONTH;
        }

        /// <summary>
        /// Аналог того, что выше только для аренды
        /// </summary>
        /// <param name="e"></param>
        [EventHandlerAttribute]
        public void ChangeRentContract(TraiderRentContractPaymentEvent e)
        {
            var airControl = _airplaneBindedMap.First(x => x.Key == e.Contract.Airplane).Value;
            airControl.numTime.Value = e.Contract.ResidualTime / TimeHelper.SECONDS_IN_MONTH;
        }

        /// <summary>
        /// Смена города у самолета
        /// </summary>
        [EventHandlerAttribute]
        public void ChangeAirCity(AirplaneMoveEvent e)
        {
            var airControl = _airplaneBindedMap.First(x => x.Key == e.Airplane).Value;
            airControl.lbCurrentCity.Text = $"Город: {e.Airplane.CurrentCity}";
        }

        /// <summary>
        /// Отписка от обработки глобальных событий, формой
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShopClosed(object sender, FormClosedEventArgs e)
        {
            AeroportServer.Instance.UnregisterEventHandler(this);
        }

    }
}
