﻿using AeroportSimulator.Server;
using AeroportSimulator.Server.Api;
using AeroportSimulator.Server.Common.Factory;
using AeroportSimulator.Server.Common.Lib;
using System;
using System.Threading;
using System.Windows.Forms;
using AeroportSimulator.Server.Api.Event;

namespace AeroportSimulator.Client.Forms
{
    public partial class CustomFlightForm : Form
    {
        /// <summary>
        /// Синглтон
        /// </summary>
        private static CustomFlightForm _instance;

        /// <summary>
        /// Геттер синглтон
        /// </summary>
        public static CustomFlightForm Instance
        {
            get
            {
                if (_instance == null || _instance.IsDisposed)
                    _instance = new CustomFlightForm();
                return _instance;
            }
        }
        /// <summary>
        /// Форма для создания своего рейса для перегонки самолетов
        /// </summary>
        public CustomFlightForm()
        {
            InitializeComponent();
            AeroportServer.Instance.RegisterEventHandler(this, SynchronizationContext.Current);

            cmbStartCity.Items.AddRange(Cities.All.ToArray());
            cmbStartCity.SelectedItem = AeroportServer.Instance.Player.Home;

            var all = AeroportServer.Instance.World.AllFlights;

            cmbEndCity.Items.AddRange(Cities.All.ToArray());
            cmbEndCity.SelectedItem = cmbEndCity.SelectedItem = all.Count > 0 
                ? all[0].SrcCity 
                : Cities.All[DateTime.Now.Second % cmbEndCity.Items.Count];

            dtmEndTime.Value = AeroportServer.Instance.CurrentDate().AddDays(2);
        }

        /// <summary>
        /// Сервер тикает меняем время в комбобоксах
        /// </summary>
        /// <param name="e"></param>
        [EventHandlerAttribute]
        public void ServerTimeChanged(ServerTickEvent e)
        {
            //FIX https://klesh-jira.atlassian.net/browse/PMA-25
            dtmEndTime.MaxDate = AeroportServer.Instance.CurrentDate().AddDays(5);
            dtmEndTime.MinDate = AeroportServer.Instance.CurrentDate().AddHours(12);
        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (cmbStartCity.SelectedItem != null && cmbEndCity.SelectedItem != null && cmbStartCity.SelectedItem != cmbEndCity.SelectedItem)
            {
                CustomFlightFactory.CreateCustomFlight(
                    (ICity) cmbStartCity.SelectedItem,
                    AeroportServer.Instance.DateTimeToTime(dtmEndTime.Value), 
                    (ICity) cmbEndCity.SelectedItem, 0);

                Close();
            }
            else
            {
                MessageBox.Show("Не все значения указаны", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RecalcDist()
        {
            var c1 = cmbStartCity.SelectedItem as ICity;
            var c2 = cmbEndCity.SelectedItem as ICity;
            if (c1 != null && c2 != null)
            {
                label3.Text = $"Расстояние: {Math.Round(c1.Position.Distance(c2.Position), 2)}м.";
            } 
            else
            {
                label3.Text = "Расстояние: ---";
            }
        }

        private void CustomFlightClosed(object sender, FormClosedEventArgs e)
        {
            AeroportServer.Instance.UnregisterEventHandler(this);
        }

        private void cmbEndCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            RecalcDist();
        }

        private void cmbStartCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            RecalcDist();
        }
    }
}
