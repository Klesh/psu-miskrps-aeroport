﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AeroportSimulator.Server.Api;

namespace AeroportSimulator.Client
{
    /// <summary>
    /// Форма для выбора чего угодно из списка
    /// </summary>
    public partial class SelectBox : Form
    {
        private static SelectBox _instance;

        private bool _isSelected;

        public SelectBox()
        {
            InitializeComponent();
            _isSelected = false;
        }

        public static T Show<T>(IEnumerable<T> list)
        {
            _instance = new SelectBox();

            foreach (var i in list)
            {
                _instance.cmbItems.Items.Add(i);
            }

            if (_instance.cmbItems.Items.Count > 0)
            {
                _instance.cmbItems.SelectedIndex = 0;
            }
            _instance.ShowDialog();
            return _instance._isSelected ? (T)_instance.cmbItems.SelectedItem : default(T);
        }

        private void btCansel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btSelect_Click(object sender, EventArgs e)
        {
            _isSelected = true;
            Close();
        }
    }
}
