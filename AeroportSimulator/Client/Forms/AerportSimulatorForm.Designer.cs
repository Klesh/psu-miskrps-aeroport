﻿namespace AeroportSimulator.Client.Forms
{
    partial class AerportSimulatorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.menuItemGame = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemNew = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemSave = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemShop = new System.Windows.Forms.ToolStripMenuItem();
            this.mapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dgvAeorport = new System.Windows.Forms.DataGridView();
            this.gBoxTablo = new System.Windows.Forms.GroupBox();
            this.numHaste = new System.Windows.Forms.TrackBar();
            this.lbHome = new System.Windows.Forms.Label();
            this.lbBalance = new System.Windows.Forms.Label();
            this.btStopResume = new System.Windows.Forms.Button();
            this.lbHaste = new System.Windows.Forms.Label();
            this.dtmData = new System.Windows.Forms.DateTimePicker();
            this.lbData = new System.Windows.Forms.Label();
            this.panelFlights = new System.Windows.Forms.FlowLayoutPanel();
            this.scSidebarWrapper = new System.Windows.Forms.SplitContainer();
            this.btEmptyFlight = new System.Windows.Forms.Button();
            this.scMainWrapper = new System.Windows.Forms.SplitContainer();
            this.tbLog = new System.Windows.Forms.TextBox();
            this.panelGlobalWrapper = new System.Windows.Forms.Panel();
            this.mainMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAeorport)).BeginInit();
            this.gBoxTablo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numHaste)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scSidebarWrapper)).BeginInit();
            this.scSidebarWrapper.Panel1.SuspendLayout();
            this.scSidebarWrapper.Panel2.SuspendLayout();
            this.scSidebarWrapper.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scMainWrapper)).BeginInit();
            this.scMainWrapper.Panel1.SuspendLayout();
            this.scMainWrapper.Panel2.SuspendLayout();
            this.scMainWrapper.SuspendLayout();
            this.panelGlobalWrapper.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenu
            // 
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemGame,
            this.menuItemSettings,
            this.menuItemHelp,
            this.menuItemShop,
            this.mapToolStripMenuItem});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(1168, 24);
            this.mainMenu.TabIndex = 0;
            this.mainMenu.Text = "menuStrip1";
            // 
            // menuItemGame
            // 
            this.menuItemGame.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemNew,
            this.menuItemSave,
            this.menuItemLoad});
            this.menuItemGame.Name = "menuItemGame";
            this.menuItemGame.Size = new System.Drawing.Size(46, 20);
            this.menuItemGame.Text = "Игра";
            // 
            // menuItemNew
            // 
            this.menuItemNew.Name = "menuItemNew";
            this.menuItemNew.Size = new System.Drawing.Size(138, 22);
            this.menuItemNew.Text = "Новая Игра";
            // 
            // menuItemSave
            // 
            this.menuItemSave.Name = "menuItemSave";
            this.menuItemSave.Size = new System.Drawing.Size(138, 22);
            this.menuItemSave.Text = "Сохранить";
            // 
            // menuItemLoad
            // 
            this.menuItemLoad.Name = "menuItemLoad";
            this.menuItemLoad.Size = new System.Drawing.Size(138, 22);
            this.menuItemLoad.Text = "Загрузить";
            // 
            // menuItemSettings
            // 
            this.menuItemSettings.Name = "menuItemSettings";
            this.menuItemSettings.Size = new System.Drawing.Size(79, 20);
            this.menuItemSettings.Text = "Настройки";
            // 
            // menuItemHelp
            // 
            this.menuItemHelp.Name = "menuItemHelp";
            this.menuItemHelp.Size = new System.Drawing.Size(65, 20);
            this.menuItemHelp.Text = "Справка";
            // 
            // menuItemShop
            // 
            this.menuItemShop.Name = "menuItemShop";
            this.menuItemShop.Size = new System.Drawing.Size(66, 20);
            this.menuItemShop.Text = "Магазин";
            this.menuItemShop.Click += new System.EventHandler(this.menuItemShop_Click);
            // 
            // mapToolStripMenuItem
            // 
            this.mapToolStripMenuItem.Name = "mapToolStripMenuItem";
            this.mapToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.mapToolStripMenuItem.Text = "Карта";
            this.mapToolStripMenuItem.Click += new System.EventHandler(this.mapToolStripMenuItem_Click);
            // 
            // dgvAeorport
            // 
            this.dgvAeorport.AllowUserToAddRows = false;
            this.dgvAeorport.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvAeorport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAeorport.Location = new System.Drawing.Point(0, 0);
            this.dgvAeorport.Name = "dgvAeorport";
            this.dgvAeorport.Size = new System.Drawing.Size(905, 341);
            this.dgvAeorport.TabIndex = 1;
            this.dgvAeorport.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.PlayerDeleteFlight);
            // 
            // gBoxTablo
            // 
            this.gBoxTablo.Controls.Add(this.numHaste);
            this.gBoxTablo.Controls.Add(this.lbHome);
            this.gBoxTablo.Controls.Add(this.lbBalance);
            this.gBoxTablo.Controls.Add(this.btStopResume);
            this.gBoxTablo.Controls.Add(this.lbHaste);
            this.gBoxTablo.Controls.Add(this.dtmData);
            this.gBoxTablo.Controls.Add(this.lbData);
            this.gBoxTablo.Dock = System.Windows.Forms.DockStyle.Top;
            this.gBoxTablo.Location = new System.Drawing.Point(3, 3);
            this.gBoxTablo.Name = "gBoxTablo";
            this.gBoxTablo.Size = new System.Drawing.Size(1162, 40);
            this.gBoxTablo.TabIndex = 2;
            this.gBoxTablo.TabStop = false;
            // 
            // numHaste
            // 
            this.numHaste.LargeChange = 1;
            this.numHaste.Location = new System.Drawing.Point(251, 13);
            this.numHaste.Maximum = 15;
            this.numHaste.Minimum = 1;
            this.numHaste.Name = "numHaste";
            this.numHaste.Size = new System.Drawing.Size(563, 45);
            this.numHaste.TabIndex = 7;
            this.numHaste.TickStyle = System.Windows.Forms.TickStyle.None;
            this.numHaste.Value = 1;
            this.numHaste.ValueChanged += new System.EventHandler(this.numHaste_ValueChanged);
            // 
            // lbHome
            // 
            this.lbHome.AutoSize = true;
            this.lbHome.Location = new System.Drawing.Point(909, 21);
            this.lbHome.Name = "lbHome";
            this.lbHome.Size = new System.Drawing.Size(66, 13);
            this.lbHome.TabIndex = 6;
            this.lbHome.Text = "Ваш город: ";
            // 
            // lbBalance
            // 
            this.lbBalance.AutoSize = true;
            this.lbBalance.Location = new System.Drawing.Point(909, 7);
            this.lbBalance.Name = "lbBalance";
            this.lbBalance.Size = new System.Drawing.Size(119, 13);
            this.lbBalance.TabIndex = 5;
            this.lbBalance.Text = "Ваш текущий баланс: ";
            // 
            // btStopResume
            // 
            this.btStopResume.Location = new System.Drawing.Point(820, 11);
            this.btStopResume.Name = "btStopResume";
            this.btStopResume.Size = new System.Drawing.Size(85, 23);
            this.btStopResume.TabIndex = 4;
            this.btStopResume.Text = "Пауза";
            this.btStopResume.UseVisualStyleBackColor = true;
            this.btStopResume.Click += new System.EventHandler(this.btStopResume_Click);
            // 
            // lbHaste
            // 
            this.lbHaste.AutoSize = true;
            this.lbHaste.Location = new System.Drawing.Point(187, 17);
            this.lbHaste.Name = "lbHaste";
            this.lbHaste.Size = new System.Drawing.Size(58, 13);
            this.lbHaste.TabIndex = 2;
            this.lbHaste.Text = "Скорость:";
            // 
            // dtmData
            // 
            this.dtmData.CustomFormat = "dd.MM.yyyy HH:mm:ss";
            this.dtmData.Enabled = false;
            this.dtmData.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtmData.Location = new System.Drawing.Point(43, 13);
            this.dtmData.Name = "dtmData";
            this.dtmData.Size = new System.Drawing.Size(140, 20);
            this.dtmData.TabIndex = 1;
            // 
            // lbData
            // 
            this.lbData.AutoSize = true;
            this.lbData.Location = new System.Drawing.Point(5, 15);
            this.lbData.Name = "lbData";
            this.lbData.Size = new System.Drawing.Size(36, 13);
            this.lbData.TabIndex = 0;
            this.lbData.Text = "Дата:";
            // 
            // panelFlights
            // 
            this.panelFlights.AutoScroll = true;
            this.panelFlights.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFlights.Location = new System.Drawing.Point(0, 0);
            this.panelFlights.Name = "panelFlights";
            this.panelFlights.Size = new System.Drawing.Size(254, 314);
            this.panelFlights.TabIndex = 3;
            // 
            // scSidebarWrapper
            // 
            this.scSidebarWrapper.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scSidebarWrapper.Location = new System.Drawing.Point(911, 0);
            this.scSidebarWrapper.Name = "scSidebarWrapper";
            this.scSidebarWrapper.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // scSidebarWrapper.Panel1
            // 
            this.scSidebarWrapper.Panel1.Controls.Add(this.btEmptyFlight);
            // 
            // scSidebarWrapper.Panel2
            // 
            this.scSidebarWrapper.Panel2.Controls.Add(this.panelFlights);
            this.scSidebarWrapper.Size = new System.Drawing.Size(254, 348);
            this.scSidebarWrapper.SplitterDistance = 30;
            this.scSidebarWrapper.TabIndex = 4;
            // 
            // btEmptyFlight
            // 
            this.btEmptyFlight.Dock = System.Windows.Forms.DockStyle.Left;
            this.btEmptyFlight.Location = new System.Drawing.Point(0, 0);
            this.btEmptyFlight.Name = "btEmptyFlight";
            this.btEmptyFlight.Size = new System.Drawing.Size(151, 30);
            this.btEmptyFlight.TabIndex = 0;
            this.btEmptyFlight.Text = "Создать пустой рейс";
            this.btEmptyFlight.UseVisualStyleBackColor = true;
            this.btEmptyFlight.Click += new System.EventHandler(this.btEmptyFlight_Click);
            // 
            // scMainWrapper
            // 
            this.scMainWrapper.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scMainWrapper.Location = new System.Drawing.Point(3, 43);
            this.scMainWrapper.Name = "scMainWrapper";
            this.scMainWrapper.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // scMainWrapper.Panel1
            // 
            this.scMainWrapper.Panel1.Controls.Add(this.dgvAeorport);
            this.scMainWrapper.Panel1.Controls.Add(this.scSidebarWrapper);
            // 
            // scMainWrapper.Panel2
            // 
            this.scMainWrapper.Panel2.Controls.Add(this.tbLog);
            this.scMainWrapper.Size = new System.Drawing.Size(1162, 466);
            this.scMainWrapper.SplitterDistance = 343;
            this.scMainWrapper.TabIndex = 5;
            // 
            // tbLog
            // 
            this.tbLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbLog.Location = new System.Drawing.Point(0, 0);
            this.tbLog.Multiline = true;
            this.tbLog.Name = "tbLog";
            this.tbLog.ReadOnly = true;
            this.tbLog.Size = new System.Drawing.Size(1162, 119);
            this.tbLog.TabIndex = 0;
            // 
            // panelGlobalWrapper
            // 
            this.panelGlobalWrapper.Controls.Add(this.scMainWrapper);
            this.panelGlobalWrapper.Controls.Add(this.gBoxTablo);
            this.panelGlobalWrapper.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelGlobalWrapper.Location = new System.Drawing.Point(0, 24);
            this.panelGlobalWrapper.Name = "panelGlobalWrapper";
            this.panelGlobalWrapper.Padding = new System.Windows.Forms.Padding(3);
            this.panelGlobalWrapper.Size = new System.Drawing.Size(1168, 512);
            this.panelGlobalWrapper.TabIndex = 6;
            // 
            // AerportSimulatorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1168, 536);
            this.Controls.Add(this.panelGlobalWrapper);
            this.Controls.Add(this.mainMenu);
            this.MainMenuStrip = this.mainMenu;
            this.Name = "AerportSimulatorForm";
            this.Text = "AerportSimulatorForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.EndGame);
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAeorport)).EndInit();
            this.gBoxTablo.ResumeLayout(false);
            this.gBoxTablo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numHaste)).EndInit();
            this.scSidebarWrapper.Panel1.ResumeLayout(false);
            this.scSidebarWrapper.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scSidebarWrapper)).EndInit();
            this.scSidebarWrapper.ResumeLayout(false);
            this.scMainWrapper.Panel1.ResumeLayout(false);
            this.scMainWrapper.Panel2.ResumeLayout(false);
            this.scMainWrapper.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scMainWrapper)).EndInit();
            this.scMainWrapper.ResumeLayout(false);
            this.panelGlobalWrapper.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem menuItemGame;
        private System.Windows.Forms.ToolStripMenuItem menuItemNew;
        private System.Windows.Forms.ToolStripMenuItem menuItemSave;
        private System.Windows.Forms.ToolStripMenuItem menuItemLoad;
        private System.Windows.Forms.ToolStripMenuItem menuItemSettings;
        private System.Windows.Forms.ToolStripMenuItem menuItemHelp;
        private System.Windows.Forms.ToolStripMenuItem menuItemShop;
        private System.Windows.Forms.DataGridView dgvAeorport;
        private System.Windows.Forms.GroupBox gBoxTablo;
        private System.Windows.Forms.DateTimePicker dtmData;
        private System.Windows.Forms.Label lbData;
        private System.Windows.Forms.Button btStopResume;
        private System.Windows.Forms.Label lbHaste;
        private System.Windows.Forms.FlowLayoutPanel panelFlights;
        private System.Windows.Forms.Label lbBalance;
        private System.Windows.Forms.SplitContainer scSidebarWrapper;
        private System.Windows.Forms.Button btEmptyFlight;
        private System.Windows.Forms.Label lbHome;
        private System.Windows.Forms.SplitContainer scMainWrapper;
        private System.Windows.Forms.TextBox tbLog;
        private System.Windows.Forms.Panel panelGlobalWrapper;
        private System.Windows.Forms.TrackBar numHaste;
        private System.Windows.Forms.ToolStripMenuItem mapToolStripMenuItem;
    }
}