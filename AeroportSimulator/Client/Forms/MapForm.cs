﻿using AeroportSimulator.Server;
using AeroportSimulator.Server.Api.Event;
using AeroportSimulator.Server.Common.Lib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AeroportSimulator.Client.Forms
{
    public partial class MapForm : Form
    {
        private Graphics _g;
        private double _scaleX;
        private double _scaleY;
        private Pen _boldRedPen = new Pen(Brushes.Red, 5);

        private void RecreateGraphics()
        {
            CleanUp();
            _g = pictureBox1.CreateGraphics();
            _scaleX = pictureBox1.Width / (Cities.MaxPosition.Abs().X - Cities.MinPosition.Abs().X);
            _scaleY = pictureBox1.Height / (Cities.MaxPosition.Abs().Y - Cities.MinPosition.Abs().Y);
        }

        private void CleanUp()
        {
            if (_g != null) _g.Dispose();
            _g = null;
        }

        public MapForm()
        {
            InitializeComponent();
            RecreateGraphics();
            AeroportServer.Instance.World.AllFlights.ForEach(f => listBox1.Items.Add(f));
            Cities.All.ForEach(c => listBox2.Items.Add(c));
            AeroportServer.Instance.RegisterEventHandler(this, SynchronizationContext.Current);
        }

        [EventHandlerAttribute]
        public void OnNewFlight(NewGeneratedFlightEvent e)
        {
            listBox1.Items.Add(e.Flight);
        }

        [EventHandlerAttribute]
        public void OnRemoveFlight(WorldRemoveFlightEvent e)
        {
            listBox1.Items.Remove(e.Flight);
        }

        [EventHandlerAttribute]
        public void OnServerTick(ServerTickEvent e)
        {
            if (_g == null) return;
            _g.Clear(Color.White);

            var radius = 5;
            foreach (var city in Cities.All)
            {
                var x = (int) (city.Position.X * _scaleX);
                var y = (int) (city.Position.Y * _scaleY);
                var brush = listBox2.SelectedItems.Contains(city) ? Brushes.Red : AeroportServer.Instance.Player.Home == city ? Brushes.Green : Brushes.Black;
                radius = brush == Brushes.Red ? 10 : 5;

                _g.FillEllipse(brush, new Rectangle(x - radius, y - radius, radius * 2, radius * 2));
                _g.DrawString(city.Name, SystemFonts.DefaultFont, Brushes.Black, new PointF(x, y - 12));
            }

            foreach (var flight in AeroportServer.Instance.World.AllFlights)
            {
                var x = (int)(flight.SrcCity.Position.X * _scaleX);
                var y = (int)(flight.SrcCity.Position.Y * _scaleY);
                var px = (int)(flight.DestCity.Position.X * _scaleX);
                var py = (int)(flight.DestCity.Position.Y * _scaleY);

                var pen = listBox1.SelectedItems.Contains(flight) ? _boldRedPen : flight.Free ? Pens.Gray : Pens.Green;
                _g.DrawLine(pen, new Point(x, y), new Point(px, py));
            }

            radius = 2;
            var lineLen = 10;
            foreach (var plane in AeroportServer.Instance.World.Player.Airplanes)
            {
                if (plane.CurrentPosition == null) continue;

                var x = (int)(plane.CurrentPosition.X * _scaleX);
                var y = (int)(plane.CurrentPosition.Y * _scaleY);
                _g.FillRectangle(Brushes.Red, new Rectangle(x - radius, y - radius, radius * 2, radius * 2));

                var pnv = (plane.PrevPosition - plane.CurrentPosition).Normalize().Negate();
                var px = (int)(pnv.X * lineLen);
                var py = (int)(pnv.Y * lineLen);
                _g.DrawLine(Pens.Red, new Point(x, y), new Point(x + px, y + py));
                _g.DrawString(plane.Type.Name, SystemFonts.DefaultFont, Brushes.Red, new PointF(x, y - 12));
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void pictureBox1_SizeChanged(object sender, EventArgs e)
        {
            RecreateGraphics();
        }

        private void MapForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            AeroportServer.Instance.UnregisterEventHandler(this);
            CleanUp();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnServerTick(null);
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnServerTick(null);
        }
    }
}
