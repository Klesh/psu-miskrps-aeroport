﻿namespace AeroportSimulator.Client.Forms
{
    partial class CustomFlightForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtmEndTime = new System.Windows.Forms.DateTimePicker();
            this.cmbStartCity = new System.Windows.Forms.ComboBox();
            this.cmbEndCity = new System.Windows.Forms.ComboBox();
            this.btCreateCustomFlight = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // dtmEndTime
            // 
            this.dtmEndTime.CustomFormat = "dd.MM.yyyy hh:mm";
            this.dtmEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtmEndTime.Location = new System.Drawing.Point(30, 27);
            this.dtmEndTime.Name = "dtmEndTime";
            this.dtmEndTime.Size = new System.Drawing.Size(138, 20);
            this.dtmEndTime.TabIndex = 1;
            // 
            // cmbStartCity
            // 
            this.cmbStartCity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStartCity.FormattingEnabled = true;
            this.cmbStartCity.Location = new System.Drawing.Point(63, 59);
            this.cmbStartCity.Name = "cmbStartCity";
            this.cmbStartCity.Size = new System.Drawing.Size(138, 21);
            this.cmbStartCity.TabIndex = 2;
            this.cmbStartCity.SelectedIndexChanged += new System.EventHandler(this.cmbStartCity_SelectedIndexChanged);
            // 
            // cmbEndCity
            // 
            this.cmbEndCity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEndCity.FormattingEnabled = true;
            this.cmbEndCity.Location = new System.Drawing.Point(63, 86);
            this.cmbEndCity.Name = "cmbEndCity";
            this.cmbEndCity.Size = new System.Drawing.Size(138, 21);
            this.cmbEndCity.TabIndex = 3;
            this.cmbEndCity.SelectedIndexChanged += new System.EventHandler(this.cmbEndCity_SelectedIndexChanged);
            // 
            // btCreateCustomFlight
            // 
            this.btCreateCustomFlight.Location = new System.Drawing.Point(40, 140);
            this.btCreateCustomFlight.Name = "btCreateCustomFlight";
            this.btCreateCustomFlight.Size = new System.Drawing.Size(128, 24);
            this.btCreateCustomFlight.TabIndex = 4;
            this.btCreateCustomFlight.Text = "Создать";
            this.btCreateCustomFlight.UseVisualStyleBackColor = true;
            this.btCreateCustomFlight.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Откуда";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Куда";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(137, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Конец реализации рейса:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Расстояние:";
            // 
            // CustomFlightForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(212, 174);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btCreateCustomFlight);
            this.Controls.Add(this.cmbEndCity);
            this.Controls.Add(this.cmbStartCity);
            this.Controls.Add(this.dtmEndTime);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "CustomFlightForm";
            this.Text = "CustomFlightForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CustomFlightClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DateTimePicker dtmEndTime;
        private System.Windows.Forms.ComboBox cmbStartCity;
        private System.Windows.Forms.ComboBox cmbEndCity;
        private System.Windows.Forms.Button btCreateCustomFlight;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
    }
}