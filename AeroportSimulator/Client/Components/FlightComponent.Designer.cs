﻿namespace AeroportSimulator.Client.Components
{
    partial class FlightComponent
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbName = new System.Windows.Forms.Label();
            this.lbFlights = new System.Windows.Forms.Label();
            this.lbDate = new System.Windows.Forms.Label();
            this.lbSrc = new System.Windows.Forms.Label();
            this.lbRegularity = new System.Windows.Forms.Label();
            this.cmbFlights = new System.Windows.Forms.ComboBox();
            this.btnAddFlight = new System.Windows.Forms.Button();
            this.lbDestination = new System.Windows.Forms.Label();
            this.lbReceipts = new System.Windows.Forms.Label();
            this.lbForfeit = new System.Windows.Forms.Label();
            this.dtmStart = new System.Windows.Forms.DateTimePicker();
            this.contextMenuFlightConditions = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.lbType = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Location = new System.Drawing.Point(4, 4);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(38, 13);
            this.lbName.TabIndex = 0;
            this.lbName.Text = "Рейс: ";
            // 
            // lbFlights
            // 
            this.lbFlights.AutoSize = true;
            this.lbFlights.Location = new System.Drawing.Point(4, 31);
            this.lbFlights.Name = "lbFlights";
            this.lbFlights.Size = new System.Drawing.Size(57, 13);
            this.lbFlights.TabIndex = 1;
            this.lbFlights.Text = "Самолет: ";
            // 
            // lbDate
            // 
            this.lbDate.AutoSize = true;
            this.lbDate.Location = new System.Drawing.Point(4, 58);
            this.lbDate.Name = "lbDate";
            this.lbDate.Size = new System.Drawing.Size(45, 13);
            this.lbDate.TabIndex = 2;
            this.lbDate.Text = "Вылет: ";
            // 
            // lbSrc
            // 
            this.lbSrc.AutoSize = true;
            this.lbSrc.Location = new System.Drawing.Point(3, 85);
            this.lbSrc.Name = "lbSrc";
            this.lbSrc.Size = new System.Drawing.Size(49, 13);
            this.lbSrc.TabIndex = 3;
            this.lbSrc.Text = "Откуда: ";
            // 
            // lbRegularity
            // 
            this.lbRegularity.AutoSize = true;
            this.lbRegularity.Location = new System.Drawing.Point(4, 193);
            this.lbRegularity.Name = "lbRegularity";
            this.lbRegularity.Size = new System.Drawing.Size(55, 13);
            this.lbRegularity.TabIndex = 5;
            this.lbRegularity.Text = "Частота: ";
            // 
            // cmbFlights
            // 
            this.cmbFlights.DisplayMember = "IAirplane.Type.Name";
            this.cmbFlights.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFlights.FormattingEnabled = true;
            this.cmbFlights.Location = new System.Drawing.Point(63, 28);
            this.cmbFlights.Name = "cmbFlights";
            this.cmbFlights.Size = new System.Drawing.Size(105, 21);
            this.cmbFlights.TabIndex = 7;
            // 
            // btnAddFlight
            // 
            this.btnAddFlight.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnAddFlight.Location = new System.Drawing.Point(176, 0);
            this.btnAddFlight.Name = "btnAddFlight";
            this.btnAddFlight.Size = new System.Drawing.Size(35, 242);
            this.btnAddFlight.TabIndex = 8;
            this.btnAddFlight.Text = "+";
            this.btnAddFlight.UseVisualStyleBackColor = true;
            this.btnAddFlight.Click += new System.EventHandler(this.btnAddFlight_Click);
            // 
            // lbDestination
            // 
            this.lbDestination.AutoSize = true;
            this.lbDestination.Location = new System.Drawing.Point(4, 112);
            this.lbDestination.Name = "lbDestination";
            this.lbDestination.Size = new System.Drawing.Size(37, 13);
            this.lbDestination.TabIndex = 9;
            this.lbDestination.Text = "Куда: ";
            // 
            // lbReceipts
            // 
            this.lbReceipts.AutoSize = true;
            this.lbReceipts.Location = new System.Drawing.Point(4, 139);
            this.lbReceipts.Name = "lbReceipts";
            this.lbReceipts.Size = new System.Drawing.Size(56, 13);
            this.lbReceipts.TabIndex = 11;
            this.lbReceipts.Text = "Выручка: ";
            // 
            // lbForfeit
            // 
            this.lbForfeit.AutoSize = true;
            this.lbForfeit.Location = new System.Drawing.Point(4, 166);
            this.lbForfeit.Name = "lbForfeit";
            this.lbForfeit.Size = new System.Drawing.Size(67, 13);
            this.lbForfeit.TabIndex = 12;
            this.lbForfeit.Text = "Неустойка: ";
            // 
            // dtmStart
            // 
            this.dtmStart.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtmStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtmStart.Location = new System.Drawing.Point(55, 55);
            this.dtmStart.Name = "dtmStart";
            this.dtmStart.ShowUpDown = true;
            this.dtmStart.Size = new System.Drawing.Size(113, 20);
            this.dtmStart.TabIndex = 13;
            // 
            // contextMenuFlightConditions
            // 
            this.contextMenuFlightConditions.Name = "contextMenuFlightConditions";
            this.contextMenuFlightConditions.Size = new System.Drawing.Size(61, 4);
            // 
            // lbType
            // 
            this.lbType.AutoSize = true;
            this.lbType.Location = new System.Drawing.Point(4, 220);
            this.lbType.Name = "lbType";
            this.lbType.Size = new System.Drawing.Size(32, 13);
            this.lbType.TabIndex = 14;
            this.lbType.Text = "Тип: ";
            // 
            // FlightComponent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ContextMenuStrip = this.contextMenuFlightConditions;
            this.Controls.Add(this.lbType);
            this.Controls.Add(this.dtmStart);
            this.Controls.Add(this.lbForfeit);
            this.Controls.Add(this.lbReceipts);
            this.Controls.Add(this.lbDestination);
            this.Controls.Add(this.btnAddFlight);
            this.Controls.Add(this.cmbFlights);
            this.Controls.Add(this.lbRegularity);
            this.Controls.Add(this.lbSrc);
            this.Controls.Add(this.lbDate);
            this.Controls.Add(this.lbFlights);
            this.Controls.Add(this.lbName);
            this.Name = "FlightComponent";
            this.Size = new System.Drawing.Size(211, 242);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.Label lbFlights;
        private System.Windows.Forms.Label lbDate;
        private System.Windows.Forms.Label lbSrc;
        private System.Windows.Forms.Label lbRegularity;
        private System.Windows.Forms.ComboBox cmbFlights;
        private System.Windows.Forms.Button btnAddFlight;
        private System.Windows.Forms.Label lbDestination;
        private System.Windows.Forms.Label lbReceipts;
        private System.Windows.Forms.Label lbForfeit;
        private System.Windows.Forms.DateTimePicker dtmStart;
        private System.Windows.Forms.ContextMenuStrip contextMenuFlightConditions;
        private System.Windows.Forms.Label lbType;
    }
}
