﻿namespace AeroportSimulator.Client.Components
{
    partial class AirplaneComponent
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbAir = new System.Windows.Forms.Label();
            this.lbPrice = new System.Windows.Forms.Label();
            this.numTime = new System.Windows.Forms.NumericUpDown();
            this.airPlaneContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuStripSpeed = new System.Windows.Forms.ToolStripTextBox();
            this.menuStripServicePrice = new System.Windows.Forms.ToolStripTextBox();
            this.menuStripFlyDistance = new System.Windows.Forms.ToolStripTextBox();
            this.menuStripFlightingSize = new System.Windows.Forms.ToolStripTextBox();
            this.menuStripPlaceCount = new System.Windows.Forms.ToolStripTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.numTime)).BeginInit();
            this.airPlaneContext.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbAir
            // 
            this.lbAir.AutoSize = true;
            this.lbAir.Location = new System.Drawing.Point(4, 4);
            this.lbAir.Name = "lbAir";
            this.lbAir.Size = new System.Drawing.Size(35, 13);
            this.lbAir.TabIndex = 0;
            this.lbAir.Text = "Имя: ";
            // 
            // lbPrice
            // 
            this.lbPrice.AutoSize = true;
            this.lbPrice.Location = new System.Drawing.Point(4, 31);
            this.lbPrice.Name = "lbPrice";
            this.lbPrice.Size = new System.Drawing.Size(0, 13);
            this.lbPrice.TabIndex = 1;
            // 
            // numTime
            // 
            this.numTime.Location = new System.Drawing.Point(95, 55);
            this.numTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numTime.Name = "numTime";
            this.numTime.Size = new System.Drawing.Size(41, 20);
            this.numTime.TabIndex = 2;
            this.numTime.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numTime.Visible = false;
            this.numTime.ValueChanged += new System.EventHandler(this.numTime_ValueChanged);
            // 
            // airPlaneContext
            // 
            this.airPlaneContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuStripSpeed,
            this.menuStripServicePrice,
            this.menuStripFlyDistance,
            this.menuStripFlightingSize,
            this.menuStripPlaceCount});
            this.airPlaneContext.Name = "airPlaneContext";
            this.airPlaneContext.Size = new System.Drawing.Size(261, 129);
            this.airPlaneContext.Text = "Информация";
            // 
            // menuStripSpeed
            // 
            this.menuStripSpeed.Name = "menuStripSpeed";
            this.menuStripSpeed.ReadOnly = true;
            this.menuStripSpeed.Size = new System.Drawing.Size(200, 23);
            // 
            // menuStripServicePrice
            // 
            this.menuStripServicePrice.Name = "menuStripServicePrice";
            this.menuStripServicePrice.ReadOnly = true;
            this.menuStripServicePrice.Size = new System.Drawing.Size(200, 23);
            // 
            // menuStripFlyDistance
            // 
            this.menuStripFlyDistance.Name = "menuStripFlyDistance";
            this.menuStripFlyDistance.ReadOnly = true;
            this.menuStripFlyDistance.Size = new System.Drawing.Size(200, 23);
            // 
            // menuStripFlightingSize
            // 
            this.menuStripFlightingSize.Name = "menuStripFlightingSize";
            this.menuStripFlightingSize.ReadOnly = true;
            this.menuStripFlightingSize.Size = new System.Drawing.Size(200, 23);
            // 
            // menuStripPlaceCount
            // 
            this.menuStripPlaceCount.Name = "menuStripPlaceCount";
            this.menuStripPlaceCount.ReadOnly = true;
            this.menuStripPlaceCount.Size = new System.Drawing.Size(200, 23);
            // 
            // AirplaneComponent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ContextMenuStrip = this.airPlaneContext;
            this.Controls.Add(this.numTime);
            this.Controls.Add(this.lbPrice);
            this.Controls.Add(this.lbAir);
            this.Name = "AirplaneComponent";
            this.Size = new System.Drawing.Size(182, 113);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.SelectAirplane);
            ((System.ComponentModel.ISupportInitialize)(this.numTime)).EndInit();
            this.airPlaneContext.ResumeLayout(false);
            this.airPlaneContext.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbAir;
        private System.Windows.Forms.Label lbPrice;
        public System.Windows.Forms.Label lbCurrentCity;
        public System.Windows.Forms.NumericUpDown numTime;
        private System.Windows.Forms.ContextMenuStrip airPlaneContext;
        private System.Windows.Forms.ToolStripTextBox menuStripSpeed;
        private System.Windows.Forms.ToolStripTextBox menuStripServicePrice;
        private System.Windows.Forms.ToolStripTextBox menuStripFlyDistance;
        private System.Windows.Forms.ToolStripTextBox menuStripFlightingSize;
        private System.Windows.Forms.ToolStripTextBox menuStripPlaceCount;
    }
}
