﻿using System;
using System.Drawing;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using AeroportSimulator.Server;
using AeroportSimulator.Server.Api;
using AeroportSimulator.Server.Api.Event;
using AeroportSimulator.Server.Common;

namespace AeroportSimulator.Client.Components
{
    public partial class AirplaneComponent : UserControl
    {
        /// <summary>
        /// Выбран ли элемент
        /// </summary>
        public bool IsSelected { get; set; }

        /// <summary>
        /// Время для аренды/лизинга
        /// </summary>
        public long Time { get; set; }

        public AirplaneComponent(object airplane, bool hasUser)
        {
            AeroportServer.Instance.RegisterEventHandler(this, SynchronizationContext.Current);
            InitializeComponent();
            Time = 1;
            IsSelected = false;
            //генерация дополнительных полей в соответствии с типом (аренда/лизинг/продажа/куплен юзером)
            if (airplane.GetType() == typeof (LeasingTerms))
            {
                var air = airplane as LeasingTerms;
                if (air != null)
                {
                    lbAir.Text += air.PlaneType.Name;
                    lbPrice.Text = $"Цена лизинга: {air.Price}$";
                    var lbTime = new Label
                    {
                        Location = new Point(4, 58),
                        Text = $"Срок в месяцах:",
                        Width = 200
                    };
                    var lbReedem = new Label
                    {
                        Location = new Point(4, 85),
                        Text = air.Redeem ? $"Можно выкупить" : $"Невыкупается",
                        Width = 200
                    };
                    if (air.Redeem)
                    {
                        
                        var lbResidualPrice = new Label
                        {
                            Location = new Point(4, 112),
                            Text = $"Цена выкупа: {air.ResidualPrice}$",
                            Width = 200

                        };
                        this.Controls.Add(lbResidualPrice);
                    }
                    numTime.ReadOnly = !hasUser;
                    numTime.Visible = true;
                    this.Controls.Add(lbTime);
                    this.Controls.Add(lbReedem);
                    SetAirplaneInfo(air.PlaneType);
                }
            }

            if (airplane.GetType() == typeof (RentTerms))
            {
                var air = airplane as RentTerms;
                if (air != null)
                {
                    lbAir.Text += air.PlaneType.Name;
                    lbPrice.Text = $"Цена аренды: {air.Price}$";
                    var lbTime = new Label
                    {
                        Location = new Point(4, 58),
                        Text = $"Срок в месяцах:",
                        Width = 200
                    };
                    numTime.ReadOnly = !hasUser;
                    numTime.Visible = true;
                    this.Height = 84;
                    this.Controls.Add(lbTime);
                    SetAirplaneInfo(air.PlaneType);
                }
            }

            if (airplane.GetType() == typeof (AirplaneType))
            {
                var air = airplane as AirplaneType;
                if (air != null)
                {
                    lbAir.Text += air.Name;
                    lbPrice.Text = $"Цена покупки: {air.Price}$";
                    this.Height = 57;
                    SetAirplaneInfo(air);
                }
            }

            //отображение купленного/взятого в аренду/лизинг самолета
            if (hasUser)
            {
                var air = airplane as IAirplane;
                lbAir.Text += $"{air.Type.Name}";
                var airState = AeroportServer.Instance.Traider.OwnershipState(AeroportServer.Instance.Player, air);
                var lbState = new Label();
                lbCurrentCity = new Label();
                switch (airState)
                {
                    case OwnershipState.IN_LEASING:
                    {
                        var contract = AeroportServer.Instance.Traider.LeasingСontract(AeroportServer.Instance.Player, air);
                        lbState.Location = new Point(4, 31);
                        lbState.Text = $"Статус: Лизинг";
                        var canBuy = contract.Terms.Redeem ? "Да" : "Нет";
                        var lbCanBuy = new Label {
                            Location = new Point(4, 58),
                            Text = $"Выкупается: {canBuy}",
                            Width = 200
                        };
                        var lbLeaseTime = new Label {
                            Location = new Point(4, 85),
                            Text = $"Срок в месяцах:",
                            Width = 200
                        };
                        numTime.Location = new Point(95, 82);
                        numTime.Enabled = false;
                        numTime.Visible = true;
                        numTime.Minimum = 0;
                        numTime.Value = contract.ResidualTime/TimeHelper.SECONDS_IN_MONTH;
                        if (contract.Terms.Redeem)
                        {
                            var lbBuyMoney = new Label
                            {
                                Location = new Point(4, 112),
                                Text = $"Цена выкупа: {contract.Terms.ResidualPrice}$",
                                Width = 200
                            };
                            lbCurrentCity.Location = new Point(4, 139);
                            this.Controls.Add(lbBuyMoney);
                            this.Height = 165;
                        }
                        else
                        {
                            lbCurrentCity.Location = new Point(4, 112);
                            this.Height = 138;
                        }
                        this.Controls.AddRange(new Control[] { lbCanBuy, lbLeaseTime, lbCurrentCity });
                    }
                        break;
                    case OwnershipState.IN_RENT:
                    {
                        var contract = AeroportServer.Instance.Traider.RentСontract(AeroportServer.Instance.Player, air);
                        lbState.Location = new Point(4, 31);
                        lbState.Text = $"Статус: Аренда";
                        var lbRentTime = new Label {
                            Location = new Point(4, 58),
                            Width = 200
                        };
                        lbRentTime.Text = $"Срок в месяцах:";
                        numTime.Enabled = false;
                        numTime.Visible = true;
                        numTime.Minimum = 0;
                        numTime.Value = contract.ResidualTime/TimeHelper.SECONDS_IN_MONTH;
                        var lbMonthMoney = new Label {Location = new Point(4, 85), Text = $"Плата: {contract.Terms.Price}$"};
                        lbCurrentCity.Location = new Point(4, 112);
                        this.Height = 138;
                        this.Controls.AddRange(new Control[] {lbRentTime, lbMonthMoney, lbCurrentCity });
                    }
                        break;

                    case OwnershipState.PURCHAESED:
                    {
                        lbPrice.Text = $"Стоимость: {air.Type.Price}$";
                        lbPrice.Width = 200;
                        lbState.Location = new Point(4, 58);
                        lbState.Text = $"Статус: cобcт-сть";
                        lbState.Width = 200;
                        lbCurrentCity.Location = new Point(4, 85);
                        this.Controls.Add(lbCurrentCity);
                        this.Height = 111;
                    }
                        break;
                    default:
                        break;
                }
                lbCurrentCity.Text = $"Город: {air.CurrentCity}";
                this.Controls.Add(lbState);
                SetAirplaneInfo(air.Type);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="airplaneType"></param>
        private void SetAirplaneInfo(IAirplaneType airplaneType)
        {
            menuStripSpeed.Text = $"Скорость: {airplaneType.Speed}";
            menuStripFlyDistance.Text = $"Дальность перелета: {airplaneType.MaxFlyDistance}";
            menuStripServicePrice.Text = $"Стоимость обслуживания: {airplaneType.ServicePrice}";
            menuStripFlightingSize.Text = $"Грузоподъемность: {airplaneType.MaxLiftingCapacity}";
            menuStripPlaceCount.Text = $"Количество мест: {airplaneType.PlaceCount}";
        }

        private void numTime_ValueChanged(object sender, EventArgs e)
        {
            Time = (long) numTime.Value;
        }

        private void SelectAirplane(object sender, MouseEventArgs e)
        {
            //TODO заморозить игру ну или генерацию самолетов -_-
            IsSelected = !IsSelected;
            this.BackColor = IsSelected ? Color.FromArgb(113, 255, 91) : SystemColors.Control;
        }

    }
}
