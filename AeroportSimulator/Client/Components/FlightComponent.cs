﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using AeroportSimulator.Server;
using AeroportSimulator.Server.Api;

namespace AeroportSimulator.Client.Components
{
    public partial class FlightComponent : UserControl
    {
        public IFlight Flight { get;}

        public IPlayer Player { get; }

        public FlightComponent(IFlight flight, IPlayer player)
        {
            InitializeComponent();
            Flight = flight;
            Player = player;

            dtmStart.ValueChanged += (a, b) =>
            {
                var d = AeroportServer.Instance.CurrentDate();
                if (d <= dtmStart.MaxDate)
                {
                    dtmStart.MinDate = d;
                }
            };

            dtmStart.MaxDate = AeroportServer.Instance.TimeToDateTime(flight.DeadlineDate);
            var min = dtmStart.MinDate.AddHours(2);
            dtmStart.Value = min <= dtmStart.MaxDate ? min : dtmStart.MaxDate;
            lbName.Text += $"{Flight.Uuid}";
            lbSrc.Text += $"{Flight.SrcCity}";
            lbDestination.Text += $"{Flight.DestCity}";
            cmbFlights.DataSource = player.Airplanes;
            var regularity = Flight.Regularity == FlightRegularity.DIRECT
                        ? "в одну сторону"
                        : "повтор: " + Flight.RepeatDayTimes + " (дней)";
            lbRegularity.Text += $"{regularity}";
            lbReceipts.Text += $"{Flight.TransferPrice}$";
            lbForfeit.Text += $"{Flight.ForfeitPrice}$";
            switch (Flight.FlightType)
            {
                case FlightType.CARGO:
                {
                    lbType.Text += $"Грузовой рейс";
                }
                    break;
                case FlightType.PASSENGER:
                {
                    lbType.Text += $"Пассажирский";
                }
                    break;
                default:
                    break;
            }

            AddFlightConditions();
        }

        /// <summary>
        /// Отображение условий по ПКМ
        /// </summary>
        private void AddFlightConditions()
        {
            foreach (var condition in Flight.Conditions)
            {
                var item = new ToolStripMenuItem
                {
                    Text = condition.ToString(),
                    Enabled = false,
                };
                contextMenuFlightConditions.Items.Add(item);
            }
        }

        /// <summary>
        /// Добавление рейса в список рейсов игрока
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddFlight_Click(object sender, EventArgs e)
        {
            if (cmbFlights.SelectedItem != null)
            {
                // принудительно обновим поле, чтобы сработало событие и учлась текущая дата на сервере
                dtmStart.Value = dtmStart.Value.AddMilliseconds(1);
                var t = AeroportServer.Instance.CurrentDate();
                if (((dtmStart.Value - t).TotalMinutes < 15 && t < dtmStart.Value) || t >= dtmStart.Value)
                {
                    var r = MessageBox.Show("До вылета рейса осталось меньше 15 минут, вы хотите взять его?\n" +
                        "В противном случае вы рискуете заплатить неустойку в разреме " + Flight.ForfeitPrice + "$",
                        "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (r != DialogResult.Yes) return;
                }

                var airplane = (IAirplane)cmbFlights.SelectedItem;
                Trace.TraceInformation($"Игрок поставил самолет: {airplane.Uuid} на рейс {Flight.Uuid} и попытался забрать рейс в свою коллекцию рейсов");
                Flight.BindToPlayer(Player, (IAirplane)cmbFlights.SelectedItem, AeroportServer.Instance.DateTimeToTime(dtmStart.Value));
            }
        }

        /// <summary>
        /// Обновление комбобкса со списком рейсов
        /// TODO - китайский способ делать публичный метод для формы, но легче делегата пофиг)
        /// </summary>
        public void UpdateData()
        {
            cmbFlights.DataSource = null;
            cmbFlights.DataSource = Player.Airplanes;
        }
    }
}
