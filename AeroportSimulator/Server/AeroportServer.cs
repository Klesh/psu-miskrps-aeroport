﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AeroportSimulator.Server.Api;
using AeroportSimulator.Server.Common;
using System.Threading;
using AeroportSimulator.Server.Api.Event;
using AeroportSimulator.Server.Common.Lib;

namespace AeroportSimulator.Server
{
    class AeroportServer : ISimulationServer
    {
        public static AeroportServer Instance { get; private set; }

        public IAirplaneTraider Traider => traider;
        public long TimeCounter => tickTimeCounter;
        public IPlayer Player => World.Player;
        public IReadOnlyList<IFlight> FreeFlights => World.AllFlights.FindAll(f => f.Free);
        public IReadOnlyList<IFlight> PlayerLights => World.AllFlights.FindAll(f => f.BindedPlayer == Player);
        public IReadOnlyList<IFlight> RunningFlights => World.AllFlights.FindAll(f => !f.Free);
        IWorld ISimulationServer.World => World;

        public World World { get; private set; } 
        
        private long tickTimeCounter = 0;
        private FlightGenerator flightGenerator;
        private long tickTimeInterval;
        private AirplaneTraider traider = new AirplaneTraider();
        private Thread serverThread;
        private long initTimeSeconds;

        public ServerParams Parameters { get; private set; }

        public AeroportServer(Player player, ServerParams parameters)
        {
            Instance = this;
            World = new World(player);
            serverThread = new Thread(ThreadProc);
            serverThread.Name = "Server Thread";

            Parameters = parameters;
            SetTickInterval(parameters.TickInterval);
            SetInitialTimeSeconds(parameters.InitialTimeSeconds);
            flightGenerator = new FlightGenerator(parameters);
        }

        private void ThreadProc()
        {
            // test
            //World.Player.AddMoney(10000000, "test");
            //var p = Traider.BuyAirplane(World.Player, AirplaneTypes.BOEING_737) as Airplane;
            //p.CurrentCity = Cities.PERM;
            //ServiceAirplane(p);
            //var f = new ForwardReverceFlight(Cities.PERM, TimeHelper.SECONDS_IN_DAY * 5, Cities.BEREZNIKI, 15000, 5);
            //World.AddFlight(f);
            //f.BindToPlayer(World.Player, p, 10);
            //f.Start();

            long lastTickCounter = 0;
            long delta;
            while (Thread.CurrentThread.IsAlive)
            {
                try
                {
                    Thread.Sleep(1000);
                } catch (ThreadInterruptedException)
                {
                    return;
                }

                tickTimeCounter += tickTimeInterval;
                delta = tickTimeCounter - lastTickCounter;
                lastTickCounter = tickTimeCounter;

                try
                {
                    flightGenerator.Tick(delta);
                    traider.Tick(delta);
                    World.Tick(delta);
                } catch (GameOverException ex)
                {
                    var bankruptEvent = new BankruptProcedureBeginEvent();
                    EventHandlerRegistry.SendEvent(bankruptEvent);
                    bankruptEvent.WaitForFinish();

                    if (Player.Money < 0)
                    {
                        EventHandlerRegistry.SendEvent(new GameOverEvent(ex.Message));
                        return;
                    }
                }


                EventHandlerRegistry.SendEvent(new ServerTickEvent(TimeCounter, delta));
            }
        }

        public bool ServiceAirplane(IAirplane plane)
        {
            if (plane.UsedInFlight != null)
            {
                TraceMessage("Попытка обслужить самолет в полете: " + plane);
                return false;
            }

            if (plane.Serviced || World.Player.ConsumeMoney(plane.Type.ServicePrice, "Обслуживание самолета: " + plane))
            {
                (plane as Airplane).Serviced = true;
                return true;
            }

            return false;
        }

        public void SetTickInterval(long interval)
        {
            tickTimeInterval = interval;
        }

        public void RegisterEventHandler(object handler, SynchronizationContext uiContext)
        {
            EventHandlerRegistry.AddHadler(handler, uiContext);
        }

        public void UnregisterEventHandler(object handler)
        {
            EventHandlerRegistry.RemoveHadler(handler);
        }

        public void Start()
        {
            serverThread.Start();
        }

        public void Stop()
        {
            serverThread.Interrupt();
        }

        public DateTime CurrentDate()
        {
            return TimeToDateTime(TimeCounter);
        }

        public DateTime TimeToDateTime(long seconds)
        {
            return new DateTime().AddSeconds(seconds).AddSeconds(initTimeSeconds).AddYears(1970);
        }

        public long DateTimeToTime(DateTime dateTime)
        {
            var epohBegin = new DateTime().AddSeconds(initTimeSeconds).AddYears(1970);
            return (long) TimeSpan.FromTicks(dateTime.Subtract(epohBegin).Ticks).TotalSeconds;
        }

        public void SetInitialTimeSeconds(long seconds)
        {
            initTimeSeconds = seconds;
        }

        internal static void TraceMessage(string msg)
        {
            if (Instance != null)
            {
                msg = $"[{Instance.CurrentDate().ToString("dd.MM.yyyy HH:mm")}]: " + msg;
            }

            EventHandlerRegistry.SendEvent(new ServerTraceMessageEvent(msg));
        }

        public void Resume()
        {
            SetTickInterval(Parameters.TickInterval);
        }

        public void Pause()
        {
            SetTickInterval(0);
        }
    }
}
