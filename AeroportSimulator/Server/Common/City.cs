﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AeroportSimulator.Server.Api.Math;
using AeroportSimulator.Server.Api;

namespace AeroportSimulator.Server.Common
{
    class City : IdEntity, ICity
    {
        public string Name { internal set; get; }
        public Vector2D Position { internal set; get; }

        public City(string name, Vector2D pos)
        {
            Name = name;
            Position = pos;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
