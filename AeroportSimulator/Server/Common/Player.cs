﻿using AeroportSimulator.Server.Api;
using AeroportSimulator.Server.Api.Event;
using AeroportSimulator.Server.Common.Lib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Common
{
    class Player : IdEntity, IPlayer
    {
        public ICity Home { get; internal set; }
        public double Money { get; internal set; }
    
        IReadOnlyList<IAirplane> IPlayer.Airplanes => Airplanes;

        public List<Airplane> Airplanes = new List<Airplane>();

        public Player(ICity home)
        {
            Home = home;
        }

        public void AddAirplane(Airplane plane)
        {
            Airplanes.Add(plane);
            EventHandlerRegistry.SendEvent(new PlayerAirplaneAddingEvent(this, plane));
        }

        public void RemoveAirplane(Airplane plane)
        {
            if (Airplanes.Remove(plane))
            {
                EventHandlerRegistry.SendEvent(new PlayerAirplaneRemovingEvent(this, plane));
            }
        }

        public bool HasAirplane(Airplane plane)
        {
            return Airplanes.Contains(plane);
        }

        public void AddMoney(double amount, string logMessage)
        {
            Money += amount;
            EventHandlerRegistry.SendEvent(new PlayerMoneyChangeEvent(this));
            AeroportServer.TraceMessage("Пополнение " + amount + "$. - " + logMessage);
        }

        public bool ConsumeMoney(double amount, string logMessage)
        {
            Money -= amount;
            EventHandlerRegistry.SendEvent(new PlayerMoneyChangeEvent(this));

            if (Money < 0)
            {
               AeroportServer.TraceMessage("Не удалось оплатить " + amount + "$ - " + logMessage);
               return false;
            } 
            else
            {
                AeroportServer.TraceMessage("Оплата " + amount + "$. - " + logMessage);
                return true;
            }
        }

        public bool HasMoney(double amount)
        {
            return Money >= amount;
        }
    }
}
