﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AeroportSimulator.Server.Api;
using AeroportSimulator.Server.Common.Lib;
using AeroportSimulator.Server.Api.Event;

namespace AeroportSimulator.Server.Common
{
    class RentTerms : IRentTerms
    {
        public IAirplaneType PlaneType { get; internal set; }
        public double MoneyPenalty { get; internal set; }
        public double Price { get; internal set; }

        public RentTerms() { }

        public RentTerms(RentTerms copy)
        {
            PlaneType = copy.PlaneType;
            Price = copy.Price;
        }
    }

    class LeasingTerms : RentTerms, ILeasingTerms
    {
        public bool Redeem { get; internal set; }
        public double ResidualPrice { get; internal set; }

        public LeasingTerms() { }

        public LeasingTerms(LeasingTerms copy) : base(copy)
        {
            Redeem = copy.Redeem;
            ResidualPrice = copy.ResidualPrice;
        }
    }

    abstract class Contract : ITraiderContract, ITickable
    {
        public IPlayer Player { get; internal set; }
        public long EndTime { get; internal set; }
        public long ResidualTime { get; internal set; }
        public long NextPayTime { get; internal set; }
        IAirplane ITraiderContract.Airplane => Airplane;
        public String Name { get; internal set; }

        public Airplane Airplane { get; set; }
        public AirplaneTraider Traider { get; set; }

        public Contract(AirplaneTraider traider, String name, long endTime)
        {
            Traider = traider;
            Name = name;
            EndTime = endTime;
        }

        public override string ToString()
        {
            return "Договор: " + Name;
        }

        public abstract void Tick(long timeDelta);
    }

    class LeasingСontract : Contract, ILeasingContract
    {
        public LeasingСontract(AirplaneTraider traider, long endTime) : base(traider, "Лизинг", endTime)
        {
        }

        public ILeasingTerms Terms { get; internal set; }

        public override void Tick(long timeDelta)
        {
            ResidualTime -= timeDelta;

            var time = AeroportServer.Instance.TimeCounter;
            Player p = Player as Player;

            if (NextPayTime <= time && ResidualTime > 0)
            {
                if (p.ConsumeMoney(Terms.Price, "Лизинг: " + Airplane))
                {
                    NextPayTime += TimeHelper.SECONDS_IN_MONTH;
                }
                else
                {
                    throw new GameOverException("Нехватило денег на оплату лизинга самолета: " + Airplane);
                }

                EventHandlerRegistry.SendEvent(new TraiderLeasingContractPaymentEvent(this));
            }

            if (ResidualTime <= 0)
            {
                var redeemed = false;

                if (Terms.Redeem && p.HasMoney(Terms.ResidualPrice))
                {
                    var req = new LeasingRedeemRequestEvent(Airplane, Terms.ResidualPrice);
                    EventHandlerRegistry.SendEvent(req);
                    req.WaitForFinish();

                    if (req.Agree)
                    {
                        p.ConsumeMoney(Terms.ResidualPrice, "Выкуп: " + Airplane);
                        redeemed = true;
                    }
                }

                if (!redeemed)
                {
                    AeroportServer.TraceMessage("Окончание лизинга для " + Airplane);
                    if (Airplane.UsedInFlight != null)
                    {
                        AeroportServer.TraceMessage("На игрока наложен штраф за невозврат самолета в срок: " + Airplane);
                        Traider.AddContract(p, new PenaltyContract(Traider)
                        {
                            Player = p,
                            Price = Terms.MoneyPenalty,
                            Reason = "Штраф за нарушение договора лизинга. Самолет не возвращен в срок"
                        });
                    }
                    else
                    {
                        p.RemoveAirplane(Airplane);
                    }
                }
            }
        }
    }

    class RentСontract : Contract, IRentContract
    {
        public RentСontract(AirplaneTraider traider, long endTime) : base(traider, "Аренда", endTime)
        {
        }

        public IRentTerms Terms { get; internal set; }

        public override void Tick(long timeDelta)
        {
            ResidualTime -= timeDelta;

            var time = AeroportServer.Instance.TimeCounter;
            Player p = Player as Player;

            if (NextPayTime <= time && ResidualTime > 0)
            {
                if (p.ConsumeMoney(Terms.Price, "Ареда: " + Airplane)) { 
                    NextPayTime += TimeHelper.SECONDS_IN_MONTH;
                }
                else
                {
                    throw new GameOverException("Нехватило денег на оплату аренды самолета: " + Airplane);
                }

                EventHandlerRegistry.SendEvent(new TraiderRentContractPaymentEvent(this));
            }

            if (ResidualTime <= 0)
            {
                AeroportServer.TraceMessage("Окончание аренды для " + Airplane);
                if (Airplane.UsedInFlight != null)
                {
                    AeroportServer.TraceMessage("На игрока наложен штраф за невозврат самолета в срок: " + Airplane);
                    Traider.AddContract(p, new PenaltyContract(Traider)
                    {
                        Player = p,
                        Price = Terms.MoneyPenalty,
                        Reason = "Штраф за нарушение арендного договора. Самолет не возвращен в срок"
                    });
                }
                else
                {
                    p.RemoveAirplane(Airplane);
                }
            }
        }
    }

    class PenaltyContract : Contract
    {
        public double Price { get; set; }
        public String Reason { get; set; }

        private long timeLost = 0;

        public PenaltyContract(AirplaneTraider traider) : base(traider, "Штраф", -1)
        {
            ResidualTime = long.MaxValue;
            EndTime = ResidualTime;
        }

        public override void Tick(long timeDelta)
        {
            timeLost += timeDelta;

            if (Airplane.UsedInFlight == null)
            {
                AeroportServer.TraceMessage("Штраф снят, самолет возвращен: " + Airplane);
                ResidualTime = 0;
                EndTime = 0;
                return;
            }

            if (timeLost >= TimeHelper.SECONDS_IN_DAY)
            {
                var p = Player as Player;
                if (!p.ConsumeMoney(Price, Reason))
                {
                   throw new GameOverException("Нехватило денег на: " + Reason);
                }
                timeLost = 0;
            }
        }
    }

    class AirplaneTraider : IAirplaneTraider, ITickable
    {
        private List<ILeasingTerms> leasingTerms = new List<ILeasingTerms>();
        private List<IRentTerms> rentTerms = new List<IRentTerms>();
        private List<IAirplaneType> airplaneShowcase = new List<IAirplaneType>();

        private Dictionary<IPlayer, List<Contract>> contractMap = new Dictionary<IPlayer, List<Contract>>();
        private Dictionary<IPlayer, List<Contract>> newContractMap = new Dictionary<IPlayer, List<Contract>>();

        private long nextUpdateTime = 0;

        public IReadOnlyList<ILeasingTerms> LeasingTerms()
        {
            return leasingTerms;
        }

        public IReadOnlyList<IRentTerms> RentTerms()
        {
            return rentTerms;
        }

        public IReadOnlyList<IAirplaneType> AirplaneShowcase()
        {
            return airplaneShowcase;
        }

        public IAirplane RentAirplane(IPlayer p, IRentTerms terms, long time)
        {
            if (!rentTerms.Contains(terms))
            {
                throw new Exception("Используйте экземпляры RentTerms полученные из метода RentTerms()");
            }

            var pl = p as Player;
            if (pl.ConsumeMoney(terms.Price, "Аренда самолета: " + terms.PlaneType))
            {
                var endTime = time * TimeHelper.SECONDS_IN_MONTH;
                var contract = new RentСontract(this, endTime + AeroportServer.Instance.TimeCounter)
                {
                    Player = p,
                    Terms = terms,
                    ResidualTime = endTime,
                    NextPayTime = AeroportServer.Instance.TimeCounter + TimeHelper.SECONDS_IN_MONTH,
                    Airplane = AirplaneTypes.Create(terms.PlaneType)
                };

                AddContract(p, contract);
                pl.AddAirplane(contract.Airplane);
                contract.Airplane.CurrentCity = pl.Home;
                contract.Airplane.CurrentPosition = pl.Home.Position;

                return contract.Airplane;
            }

            return null;
        }

        public IAirplane LeaseAirplane(IPlayer p, ILeasingTerms terms, long time)
        {
            if (!leasingTerms.Contains(terms))
            {
                throw new Exception("Используйте экземпляры LeasingTerms полученные из метода LeasingTerms()");
            }

            var pl = p as Player;
            if (pl.ConsumeMoney(terms.Price, "Лизинг самолета: " + terms.PlaneType))
            {
                var endTime = time * TimeHelper.SECONDS_IN_MONTH;
                var contract = new LeasingСontract(this, endTime + AeroportServer.Instance.TimeCounter)
                {
                    Player = p,
                    Terms = terms,
                    ResidualTime = time * TimeHelper.SECONDS_IN_MONTH,
                    NextPayTime = AeroportServer.Instance.TimeCounter + TimeHelper.SECONDS_IN_MONTH,
                    Airplane = AirplaneTypes.Create(terms.PlaneType)
                };

                AddContract(p, contract);
                pl.AddAirplane(contract.Airplane);
                contract.Airplane.CurrentCity = pl.Home;
                contract.Airplane.CurrentPosition = pl.Home.Position;

                return contract.Airplane;
            }

            return null;
        }

        public IAirplane BuyAirplane(IPlayer p, IAirplaneType type)
        {
            Player pl = p as Player;
            if (pl.ConsumeMoney(type.Price, "Покупка самолета: " + type))
            {
                var plane = AirplaneTypes.Create(type);
                plane.CurrentCity = pl.Home;
                pl.AddAirplane(plane);
                return plane;
            }

            return null;
        }

        public bool SellAirplane(IPlayer p, IAirplane plane)
        {
            var pl = p as Player;
            if (pl.HasAirplane(plane as Airplane))
            {
                if (contractMap.ContainsKey(p) && contractMap[p].Any(data => data.Airplane == plane))
                {
                    AeroportServer.TraceMessage("Игрок попытался продать контрактный самолет");
                    return false;
                }

                pl.RemoveAirplane(plane as Airplane);
                pl.AddMoney(plane.Type.Price / 2, "Продажа: " + plane);
                return true;
            }

            return false;
        }

        public void Tick(long timeDelta)
        {
            ProcessContracts(timeDelta);
            GenShowcaseContent();
        }

        private void GenShowcaseContent()
        {
            var config = AeroportServer.Instance.Parameters;
            var time = AeroportServer.Instance.TimeCounter;

            if (time >= nextUpdateTime)
            {
                Random rnd = new Random();
                
                nextUpdateTime = time + config.TraiderShowcaseUpdateIntervalTime;
                leasingTerms.Clear();
                rentTerms.Clear();
                airplaneShowcase.Clear();

                // генерируем для простой покупки
                for (int i = 0; i < rnd.Next(AirplaneTypes.All.Count); i++)
                {
                    var arType = AirplaneTypes.All[rnd.Next(AirplaneTypes.All.Count)];
                    if (!airplaneShowcase.Contains(arType))
                    {
                        airplaneShowcase.Add(arType);
                    }
                }

                // генерируем для лизинга
                for (int i = 0; i < rnd.Next(AirplaneTypes.All.Count); i++)
                {
                    var arType = AirplaneTypes.All[rnd.Next(AirplaneTypes.All.Count)];

                    // если уже есть такое предложение, то пропускаем
                    if (leasingTerms.Any(t => t.PlaneType == arType)) continue;

                    leasingTerms.Add(new LeasingTerms
                    {
                        PlaneType = arType,
                        Price = Math.Round(arType.Price * config.TraiderLeasingPriceCoefficient, 2),
                        Redeem = rnd.Next(2) == 0,
                        ResidualPrice = Math.Round(arType.Price * config.TraiderLeasingResidualPriceCoefficient, 2),
                        MoneyPenalty = Math.Round(arType.Price * config.TraiderLeasingPenaltyPriceCoefficient, 2)
                    });
                }

                // генерируем для аренды
                for (int i = 0; i < rnd.Next(AirplaneTypes.All.Count); i++)
                {
                    var arType = AirplaneTypes.All[rnd.Next(AirplaneTypes.All.Count)];

                    // если уже есть такое предложение, то пропускаем
                    if (rentTerms.Any(t => t.PlaneType == arType)) continue;

                    rentTerms.Add(new RentTerms
                    {
                        PlaneType = arType,
                        Price = Math.Round(arType.Price * config.TraiderRentPriceCoefficient, 2),
                        MoneyPenalty = Math.Round(arType.Price * config.TraiderRentPenaltyPriceCoefficient, 2)
                    });
                }

                // уведомляем клиент
                EventHandlerRegistry.SendEvent(new TraiderShowcaseUpdateEvent(leasingTerms, rentTerms, airplaneShowcase));
            }
        }

        public void AddContract(IPlayer p, Contract c)
        {
            List<Contract> cs;
            if (!newContractMap.TryGetValue(p, out cs))
            {
                newContractMap[p] = (cs = new List<Contract>());
            }

            cs.Add(c);
        }

        private void AcceptNewContracts()
        {
            List<Contract> cs;
            foreach (var nc in newContractMap)
            {
                if (!contractMap.TryGetValue(nc.Key, out cs))
                {
                    contractMap[nc.Key] = (cs = new List<Contract>());
                }

                cs.AddRange(nc.Value);
            }

            newContractMap.Clear();
        }

        private void ProcessContracts(long timeDelta)
        {
            foreach (var pair in contractMap)
            {
                foreach (var contract in pair.Value)
                {
                    contract.Tick(timeDelta);
                }

                pair.Value.RemoveAll(c => c.ResidualTime <= 0);
            }

            AcceptNewContracts();
        }

        public OwnershipState OwnershipState(IPlayer p, IAirplane plane)
        {
            List<Contract> cs;
            if (contractMap.TryGetValue(p, out cs) || newContractMap.TryGetValue(p, out cs)) {
                Contract c;
                if ((c = cs.Find(cc => cc.Airplane == plane)) != null)
                {
                    return c is LeasingСontract ? Api.OwnershipState.IN_LEASING : Api.OwnershipState.IN_RENT;
                }
            }

            return Api.OwnershipState.PURCHAESED;
        }

        public ILeasingContract LeasingСontract(IPlayer p, IAirplane plane)
        {
            List<Contract> cs;
            if (contractMap.TryGetValue(p, out cs) || newContractMap.TryGetValue(p, out cs))
            {
                return cs.Find(c => c.Airplane == plane) as ILeasingContract;
            }

            return null;
        }

        public IRentContract RentСontract(IPlayer p, IAirplane plane)
        {
            List<Contract> cs;
            if (contractMap.TryGetValue(p, out cs) || newContractMap.TryGetValue(p, out cs))
            {
                return cs.Find(c => c.Airplane == plane) as IRentContract;
            }

            return null;
        }
    }
}
