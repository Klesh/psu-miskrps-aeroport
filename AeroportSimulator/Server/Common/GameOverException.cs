﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Common
{
    class GameOverException : Exception
    {
        public GameOverException(string reason) : base(reason)
        {
        }
    }
}
