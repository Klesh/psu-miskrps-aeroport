﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Common
{
    public static class TimeHelper
    {
        public const int SECONDS_IN_DAY = 24 * 60 * 60;
        public const long SECONDS_IN_MONTH = 31 * SECONDS_IN_DAY;
    }
}
