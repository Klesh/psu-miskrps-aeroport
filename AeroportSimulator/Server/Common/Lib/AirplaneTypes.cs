﻿using AeroportSimulator.Server.Api;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Common.Lib
{
    public sealed class AirplaneTypes
    {
        /// <summary>
        /// Пассажирский самолет для авиалиний малой протяженности.
        /// </summary>
        public static readonly IAirplaneType TU_134 = new AirplaneType("Ту-134", 1300000)
        {
            Speed = 850,
            ServicePrice = 9000,
            MaxFlyDistance = 2800,
            MaxLiftingCapacity = 47000,
            PlaceCount = 80
        };

        /// <summary>
        /// Пассажирский самолет для авиалиний средней протяженности.
        /// </summary>
        public static readonly IAirplaneType TU_154 = new AirplaneType("Ту-154", 1500000)
        {
            Speed = 950,
            ServicePrice = 12000,
            MaxFlyDistance = 3000,
            MaxLiftingCapacity = 47000,
            PlaceCount = 150
        };

        /// <summary>
        /// Пассажирский самолет для авиалиний средней протяженности.
        /// </summary>
        public static readonly IAirplaneType TU_204 = new AirplaneType("Ту-204", 1700000)
        {
            Speed = 850,
            ServicePrice = 18000,
            MaxFlyDistance = 10000,
            MaxLiftingCapacity = 107900,
            PlaceCount = 30
        };

        /// <summary>
        /// Пассажирский самолет для авиалиний средней (A310-200) и большой (A310-300) протяженности.
        /// </summary>
        public static readonly IAirplaneType AIRBUS_A310 = new AirplaneType("Airbus A310", 2000000)
        {
            Speed = 858,
            ServicePrice = 20000,
            MaxFlyDistance = 3500,
            MaxLiftingCapacity = 164000,
            PlaceCount = 180
        };

        /// <summary>
        /// Пассажирский самолет для авиалиний малой и средней протяженности.
        /// </summary>
        public static readonly IAirplaneType AIRBUS_A320 = new AirplaneType("Airbus A320", 2500000)
        {
            Speed = 853,
            ServicePrice = 15000,
            MaxFlyDistance = 3700,
            MaxLiftingCapacity = 68000,
            PlaceCount = 120
        };

        /// <summary>
        /// Пассажирский самолет для авиалиний большой протяженности.
        /// </summary>
        public static readonly IAirplaneType AIRBUS_A330 = new AirplaneType("Airbus A330", 2900000)
        {
            Speed = 925,
            ServicePrice = 25000,
            MaxFlyDistance = 11900,
            MaxLiftingCapacity = 55000,
            PlaceCount = 440
        };

        /// <summary>
        /// Пассажирский самолет для авиалиний малой и средней протяженности.
        /// </summary>
        public static readonly IAirplaneType BOEING_737 = new AirplaneType("Boeing-737", 3100000)
        {
            Speed = 793 ,
            ServicePrice = 10000,
            MaxFlyDistance = 2518,
            MaxLiftingCapacity = 52800,
            PlaceCount = 114
        };

        /// <summary>
        /// Пассажирский самолет для авиалиний большой протяженности.
        /// </summary>
        public static readonly IAirplaneType BOEING_747 = new AirplaneType("Boeing-747", 3500000)
        {
            Speed = 917,
            ServicePrice = 11000,
            MaxFlyDistance = 7163,
            MaxLiftingCapacity = 45000,
            PlaceCount = 425
        };

        /// <summary>
        /// Пассажирский самолет для авиалиний средней протяженности.
        /// </summary>
        public static readonly IAirplaneType BOEING_767 = new AirplaneType("Boeing-767", 3850000)
        {
            Speed = 873,
            ServicePrice = 26000,
            MaxFlyDistance = 4000,
            MaxLiftingCapacity = 34000,
            PlaceCount = 252
        };

        /// <summary>
        /// Пассажирский самолет для авиалиний большой протяженности.
        /// </summary>
        public static readonly IAirplaneType BOEING_777 = new AirplaneType("Boeing-777", 4150000)
        {
            Speed = 891,
            ServicePrice = 19000,
            MaxFlyDistance = 7406,
            MaxLiftingCapacity = 35000,
            PlaceCount = 235
        };

        private static Dictionary<IAirplaneType, Func<Airplane>> factoryMap = new Dictionary<IAirplaneType, Func<Airplane>>();

        public static ReadOnlyCollection<IAirplaneType> All { get; private set; }

        static AirplaneTypes()
        {
            factoryMap[TU_134] = () => new Airplane(TU_134);
            factoryMap[TU_154] = () => new Airplane(TU_154);
            factoryMap[TU_204] = () => new Airplane(TU_204);
            factoryMap[AIRBUS_A310] = () => new Airplane(AIRBUS_A310);
            factoryMap[AIRBUS_A320] = () => new Airplane(AIRBUS_A320);
            factoryMap[AIRBUS_A330] = () => new Airplane(AIRBUS_A330);
            factoryMap[BOEING_737] = () => new Airplane(BOEING_737);
            factoryMap[BOEING_747] = () => new Airplane(BOEING_747);
            factoryMap[BOEING_767] = () => new Airplane(BOEING_767);
            factoryMap[BOEING_777] = () => new Airplane(BOEING_777);

            All = new ReadOnlyCollection<IAirplaneType>(factoryMap.Keys.ToList());
        }

        static internal Airplane Create(IAirplaneType type)
        {
            Func<Airplane> factory;
            if (factoryMap.TryGetValue(type, out factory))
            {
                return factory.Invoke();
            }

            throw new Exception("Неизвестный тип самолета для создания");
        }
    }
}
