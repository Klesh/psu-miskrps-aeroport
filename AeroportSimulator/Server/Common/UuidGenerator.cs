﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Common
{
    class UuidGenerator
    {
        private static long uuidCounter = 0;

        public static long GenUUID()
        {
            return uuidCounter++;
        }
    }
}
