﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AeroportSimulator.Server.Api;
using AeroportSimulator.Server.Api.Math;
using AeroportSimulator.Server.Common.Lib;
using AeroportSimulator.Server.Api.Event;

namespace AeroportSimulator.Server.Common
{
    class FlightGenerator : ITickable
    {
        private Random rand = new Random();
        private long nextGenTime;
        private ServerParams parameters;

        public FlightGenerator(ServerParams parameters)
        {
            this.parameters = parameters;
        }

        /// <summary>
        /// https://www.wolframalpha.com/input/?i=plot+1%2F(1*sqrt(2pi))*exp(-(x+-+0)%5E2%2F(2*1%5E2))%2B0.6
        /// </summary>
        private double NormalDistFunc(double x) {
            var scale = 1D;
            var offset = 0D;
            var yUp = 0.6D;
            return 1D / (scale * Math.Sqrt(2 * Math.PI)) * Math.Exp(-Math.Pow((x - offset), 2D) / (2D * Math.Pow(scale, 2))) + yUp;
        }

        public void Tick(long timeDelta)
        {
            long time = AeroportServer.Instance.TimeCounter;
            if (time >= nextGenTime)
            {
                ICity from = Cities.All[rand.Next(Cities.All.Count)];
                ICity to = Cities.All[rand.Next(Cities.All.Count)];

                // если так получилось что сгенерировалось в один и тот же город, то пропускаем тик, пускай в следующем сгенится нормально
                if (from == to) return;

                nextGenTime = time + rand.Next(parameters.FlightGenMinGenIntervalTime, parameters.FlightGenMaxGenIntervalTime);
                long end = time + TimeHelper.SECONDS_IN_DAY + rand.Next(parameters.FlightGenMinLiveTime, parameters.FlightGenMaxLiveTime);
                end -= end % TimeHelper.SECONDS_IN_DAY;

                DirectFlight f;

                switch (rand.Next(2))
                {
                    case 0: f = new DirectFlight(from, end, to, 0);
                        break;
                    default: f = new ForwardReverceFlight(from, end, to, 0, rand.Next(3, 14));
                        break;
                }

                f.ForfeitPrice = rand.Next(parameters.FlightGenMinForfeitPrice, parameters.FlightGenMaxForfeitPrice);

                switch (rand.Next(2))
                {
                    case 0:
                        {
                            int c = rand.Next(parameters.FlightGenMinRequiredPassengersCount, parameters.FlightGenMaxRequiredPassengersCount);
                            f.FlightType = FlightType.PASSENGER;
                            f.Conditions.Add(new FlightMinPassengersCountCondition(c));
                            var ticketPrice = parameters.FlightGenMaxTicketPrice;
                            var dayProgress = AeroportServer.Instance.CurrentDate().TimeOfDay.Seconds / (double) TimeHelper.SECONDS_IN_DAY;
                            dayProgress = dayProgress * 4 - 2; // to range -2 to 2
                            f.TransferPrice = Math.Round(ticketPrice * c * NormalDistFunc(dayProgress), 2);
                            break;
                        }
                    case 1:
                        {
                            int c = rand.Next(parameters.FlightGenMinRequiredLiftingCapacity, parameters.FlightGenMaxRequiredLiftingCapacity);
                            f.FlightType = FlightType.CARGO;
                            f.Conditions.Add(new FlightMinLiftingСapacityCondition(c));
                            f.TransferPrice = parameters.FlightGenCargoFlightPrice;
                            break;
                        }
                }

                EventHandlerRegistry.SendEvent(new NewGeneratedFlightEvent(f));
                AeroportServer.Instance.World.AddFlight(f);
            }
        }
    }
}
