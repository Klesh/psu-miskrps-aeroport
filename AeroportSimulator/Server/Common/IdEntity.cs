﻿using AeroportSimulator.Server.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Common
{
    abstract class IdEntity : IIdEntity
    {
        public long Uuid { get; } = UuidGenerator.GenUUID();

        public override bool Equals(object obj)
        {
            if (base.Equals(obj)) return true;
            if (!obj.GetType().Equals(GetType())) return false;

            IdEntity that = (IdEntity)obj;
            return that.Uuid == this.Uuid;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode() + Uuid.GetHashCode();
        }
    }
}
