﻿using AeroportSimulator.Server.Api;
using AeroportSimulator.Server.Api.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Common
{
    class World : IWorld, ITickable
    {
        IReadOnlyList<IFlight> IWorld.AllFlights => AllFlights;
        IPlayer IWorld.Player => Player;

        public List<DirectFlight> AllFlights { get; } = new List<DirectFlight>();
        public Player Player { get; }

        private List<DirectFlight> removingSchedule = new List<DirectFlight>();
        private List<DirectFlight> addingSchedule = new List<DirectFlight>();

        public World(Player player)
        {
            Player = player;
        }

        public void AddFlight(DirectFlight flight)
        {
            addingSchedule.Add(flight);
        }

        public void RemoveFlight(DirectFlight flight)
        {
            removingSchedule.Add(flight);
        }

        public void Tick(long timeDelta)
        {
            AllFlights.ForEach(f => f.Tick(timeDelta));
            
            foreach (var f in AllFlights)
            {
                if (f.Overdue || f.Finished)
                {
                    if (f.Overdue && !f.Finished && !f.ForfeitPayed) {
                        if (f.PayForfeit())
                        {
                            EventHandlerRegistry.SendEvent(new OverdueFlightEvent(f));
                        }
                        else
                        {
                            throw new GameOverException("У игрока не хватило денег на оплату неустойки за просроченный рейс: " + f.ToString());
                        }
                    }

                    RemoveFlight(f);
                }
            }

            if (Player.Money < 0)
            {
                throw new GameOverException("Баланс игрока стал меньше нуля");
            }

            removingSchedule.ForEach(f => {
                AllFlights.Remove(f);
                EventHandlerRegistry.SendEvent(new WorldRemoveFlightEvent(f));
            });
            removingSchedule.Clear();

            addingSchedule.ForEach(f => {
                AllFlights.Add(f);
                EventHandlerRegistry.SendEvent(new WorldAddFlightEvent(f));
            });
            addingSchedule.Clear();
        }
    }
}
