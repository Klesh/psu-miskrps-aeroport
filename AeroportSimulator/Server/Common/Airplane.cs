﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AeroportSimulator.Server.Api;
using AeroportSimulator.Server.Api.Math;
using AeroportSimulator.Server.Common.Lib;

namespace AeroportSimulator.Server.Common
{
    class Airplane : IdEntity, IAirplane
    {
        public double ServicePrice { get; internal set; }
        public bool Serviced { get; internal set; }
        public Vector2D PrevPosition { get; internal set; }
        public Vector2D CurrentPosition { get { return currPos;} internal set { PrevPosition = currPos; currPos = value; } }
        public ICity CurrentCity { get { return currCity; } internal set { currCity = value; CurrentPosition = value.Position; } }
        public IAirplaneType Type { get; internal set; }
        public IFlight UsedInFlight { get; internal set; }

        IAirplaneType IAirplane.Type => Type;
        bool IAirplane.NeedService => !Serviced;

        public int BusyPlacesCount { get; internal set; }
        public int CurrentWeight { get; internal set; }

        private Vector2D currPos = new Vector2D(0,0);
        private ICity currCity;

        public Airplane(IAirplaneType type)
        {
            Type = type;
        }

        public override string ToString()
        {
            return Type.Name;
        }
    }
}
