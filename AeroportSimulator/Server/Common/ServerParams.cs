﻿using AeroportSimulator.Server.Api;
using AeroportSimulator.Server.Common.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Common
{
    /// <summary>
    /// Класс с параметрами сервера
    /// </summary>
    public class ServerParams
    {
        /// <summary>
        /// Дом игрока
        /// </summary>
        public ICity Home { get; set; } = Cities.All[DateTime.Now.Millisecond % Cities.All.Count];

        /// <summary>
        /// Начальный баланс игрока
        /// </summary>
        public double InitialMoney { get; set; } = 50000;

        /// <summary>
        /// Время в секундах одного тика
        /// </summary>
        public int TickInterval { get; set; } = 1;

        /// <summary>
        /// Начальный отступ времени в секундах
        /// </summary>
        public long InitialTimeSeconds { get; set; } = 943920000L;

        /// <summary>
        /// Цена за грузовой рейс
        /// </summary>
        public int FlightGenCargoFlightPrice { get; set; } = 25000;

        /// <summary>
        /// Максимальная цена за билет для пассажира
        /// </summary>
        public int FlightGenMaxTicketPrice { get; set; } = 30000;

        /// <summary>
        /// Минимальная неустойка рейса
        /// </summary>
        public int FlightGenMinForfeitPrice { get; set; } = 3500;

        /// <summary>
        /// Максимальная неустойка рейса
        /// </summary>
        public int FlightGenMaxForfeitPrice { get; set; } = 10000;

        /// <summary>
        /// Минимальный промежуток между генерациями рейсов
        /// </summary>
        public int FlightGenMinGenIntervalTime { get; set; } = 15 * 60 * 60;
        
        /// <summary>
        /// Максимальный промежуток между генерациями рейсов
        /// </summary>
        public int FlightGenMaxGenIntervalTime { get; set; } = 35 * 60 * 60;

        /// <summary>
        /// Минимальное время исполнения рейса
        /// </summary>
        public int FlightGenMinLiveTime { get; set; } = 24 * 60 * 60;

        /// <summary>
        /// Максимальное время исполнения рейса
        /// </summary>
        public int FlightGenMaxLiveTime { get; set; } = 10 * 24 * 60 * 60;

        /// <summary>
        /// Минимальное требуемое количество пассажиров на пассажирский рейс
        /// </summary>
        public int FlightGenMinRequiredPassengersCount { get; set; } = 30;

        /// <summary>
        /// Максиммальное требуемое количество пассажиров на пассажирский рейс
        /// </summary>
        public int FlightGenMaxRequiredPassengersCount { get; set; } = 80;

        /// <summary>
        /// Минимальная требуемая грузоподъемность самолета для грузового рейса
        /// </summary>
        public int FlightGenMinRequiredLiftingCapacity { get; set; } = 5000;

        /// <summary>
        /// Максимальная требуемая грузоподъемность самолета для грузового рейса
        /// </summary>
        public int FlightGenMaxRequiredLiftingCapacity { get; set; } = 70000;

        /// <summary>
        /// Интервал обновления ассортимента магазна. По умолчанию - каждую неделю
        /// </summary>
        public int TraiderShowcaseUpdateIntervalTime { get; set; } = 7 * 24 * 60 * 60;

        /// <summary>
        /// Число на которое умножается цена самолета для месячного платежа лизинга.
        /// ЦенаЗаЛизингВМесяц = ЦенаСамолета * Число
        /// </summary>
        public double TraiderLeasingPriceCoefficient { get; set; } = 1D / 150D;

        /// <summary>
        /// Число на которое умножается цена самолета для остаточной платежа.
        /// ОстаточнаяСумма = ЦенаСамолета * Число
        /// </summary>
        public double TraiderLeasingResidualPriceCoefficient { get; set; } = 1D / 10D;

        /// <summary>
        /// Число на которое умножается цена самолета для месячного платежа аренды.
        /// ЦенаЗаАрендуВМесяц = ЦенаСамолета * Число
        /// </summary>
        public double TraiderRentPriceCoefficient { get; set; } = 1D / 100D;

        /// <summary>
        /// Число на которое умножается цена самолетя для штрафа за лизиг
        /// </summary>
        public double TraiderLeasingPenaltyPriceCoefficient { get; set; }

        /// <summary>
        /// Число на которое умножается цена самолетя для штрафа за аренду
        /// </summary>
        public double TraiderRentPenaltyPriceCoefficient { get; set; }
    }
}
