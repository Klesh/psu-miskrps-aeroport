﻿using AeroportSimulator.Server.Api;
using AeroportSimulator.Server.Api.Event;
using AeroportSimulator.Server.Common.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Common
{
    class FlightMinPassengersCountCondition : IFlightCondition
    {
        private int minimalCount;

        public FlightMinPassengersCountCondition(int minimalCount)
        {
            this.minimalCount = minimalCount;
        }

        public bool Accept(IFlight flight)
        {
            return flight.Airplane.Type.PlaceCount >= minimalCount;
        }

        public override string ToString()
        {
            return "Минимальное кол-во пассажиров: " + minimalCount;
        }
    }

    class FlightMinLiftingСapacityCondition : IFlightCondition
    {
        private int minimalWeight;

        public FlightMinLiftingСapacityCondition(int minimalWeight)
        {
            this.minimalWeight = minimalWeight;
        }

        public bool Accept(IFlight flight)
        {
            return flight.Airplane.Type.MaxLiftingCapacity >= minimalWeight;
        }

        public override string ToString()
        {
            return "Минимальная грузоподъемность: " + minimalWeight;
        }
    }

    class FlightMinFlyDistanceCondition : IFlightCondition
    {
        private double minDist;

        public FlightMinFlyDistanceCondition(double minDist)
        {
            this.minDist = minDist;
        }

        public bool Accept(IFlight flight)
        {
            return flight.Airplane.Type.MaxFlyDistance >= minDist;
        }

        public override string ToString()
        {
            return "Минимальная длинна перелета самолета: " + minDist;
        }
    }

    class FlightServicedAirplaneCondition : IFlightCondition
    {
        public bool Accept(IFlight flight)
        {
            return flight.Airplane.Serviced || flight.Airplane.UsedInFlight == flight;
        }

        public override string ToString()
        {
            return "Самолет должен быть обслужен";
        }
    }

    class FlightAirplaneSrcCityExistsCondition : IFlightCondition
    {
        public bool Accept(IFlight flight)
        {
            return flight.SrcCity == flight.Airplane.CurrentCity;
        }

        public override string ToString()
        {
            return "Выбраный самолет должен находится в городе отправки";
        }
    }

    class FlightAirplaneOwnedByPlayerCondition : IFlightCondition
    {
        public bool Accept(IFlight flight)
        {
            return flight.BindedPlayer != null ? flight.BindedPlayer.Airplanes.Contains(flight.Airplane) : false;
        }

        public override string ToString()
        {
            return "Выбраный самолет должен находится в собственности игрока";
        }
    }

    class FlightAirplaneFreeCondition : IFlightCondition
    {
        public bool Accept(IFlight flight)
        {
            return flight.Airplane.UsedInFlight == null || flight.Airplane.UsedInFlight == flight;
        }

        public override string ToString()
        {
            return "Выбраный самолет должен быть свободен";
        }
    }

    class DirectFlight : IdEntity, IFlight, ITickable
    {
        IAirplane IFlight.Airplane => Airplane;
        IPlayer IFlight.BindedPlayer => BindedPlayer;
        IReadOnlyList<IFlightCondition> IFlight.Conditions => Conditions;

        public bool Overdue =>
            AeroportServer.Instance.TimeCounter > DeadlineDate
            || (DepartureTime > 0 && !started && AeroportServer.Instance.TimeCounter > DepartureTime);

        public bool Free => !binded;
        public bool Finished => finished;
        public long ArrivalTime { get; internal set; }
        public long DepartureTime { get; internal set; }
        public ICity DestCity { get; internal set; }
        public FlightType FlightType { get; internal set; } = FlightType.PASSENGER;
        public double ForfeitPrice { get; internal set; }
        public double TransferPrice { get; internal set; }
        public FlightRegularity Regularity { get; internal set; } = FlightRegularity.DIRECT;
        public ICity SrcCity { get; internal set; }
        public long DeadlineDate { get; internal set; }
        public FlightState State { get; internal set; } = FlightState.BIND_WAIT;
        public double Progress { get; internal set; } = 0D;
        public int RepeatDayTimes { get; internal set; } = 0;

        private bool started;
        private bool finished;
        private bool binded;

        private double distanceBetweenCities;
        public bool ForfeitPayed { get; private set; }
        public Airplane Airplane { get; set; }
        public Player BindedPlayer { get; set; }
        public List<IFlightCondition> Conditions { get; } = new List<IFlightCondition>();


        public DirectFlight(ICity srcCity, long endDate, ICity destCity, double price)
        {
            DeadlineDate = endDate;
            SrcCity = srcCity;
            DestCity = destCity;
            TransferPrice = price;

            distanceBetweenCities = DestCity.Position.Distance(srcCity.Position);

            Conditions.Add(new FlightMinFlyDistanceCondition(distanceBetweenCities));
            Conditions.Add(new FlightServicedAirplaneCondition());
            Conditions.Add(new FlightAirplaneSrcCityExistsCondition());
            Conditions.Add(new FlightAirplaneOwnedByPlayerCondition());
            Conditions.Add(new FlightAirplaneFreeCondition());
        }

        private void SetState(FlightState state)
        {
            State = state;
            EventHandlerRegistry.SendEvent(new FlightStatusChangeEvent(this));
        }

        public void BindToPlayer(IPlayer p, IAirplane plane, long departureTime)
        {
            BindedPlayer = p as Player;
            Airplane = plane as Airplane;
            binded = true;
            DepartureTime = departureTime;
            ArrivalTime = DepartureTime + ComputeFlyingTimeForAirplane(plane);
            SetState(FlightState.PLANING);

            EventHandlerRegistry.SendEvent(new BindFlightEvent(this));
            AeroportServer.TraceMessage("Рейс привязан к игроку: " + this);
        }

        public long ComputeFlyingTimeForAirplane(IAirplane plane)
        {
            return (long)((SrcCity.Position.Distance(DestCity.Position) / plane.Type.Speed) * 60D * 60D);
        }

        public bool ReplaceAirplane(IAirplane newPlane)
        {
            if (!binded) return false;
            AeroportServer.TraceMessage("Замена самолета " + Airplane + " на " + newPlane);
            Airplane = newPlane as Airplane;
            return true;
        }

        public bool PayForfeit()
        {
            if (!ForfeitPayed && ForfeitPrice > 0 && BindedPlayer != null && !Finished)
            {
                ForfeitPayed = true;
                // принудительно платим неустойку
                BindedPlayer.AddMoney(-ForfeitPrice, "Неустойка за " + this);
                return BindedPlayer.Money >= 0;
            }

            return true;
        }

        public void UnBind()
        {
            PayForfeit();
            BindedPlayer = null;
            binded = false;

            EventHandlerRegistry.SendEvent(new UnBindFlightEvent(this));
        }

        public void Start()
        {
            if (started) return;

            if (!binded)
            {
                throw new Exception("Рейс должен быть забинден на игрока и самолет до старта");
            }

            if (!CanStart())
            {
                throw new Exception("Рейс не может быть начат, не все условия рейса выполнены для начала");
            }

            started = true;
            Airplane.CurrentCity = SrcCity;
            Airplane.Serviced = false;
            Airplane.UsedInFlight = this;
            SetState(FlightState.DEPARTURE_WAIT);

            EventHandlerRegistry.SendEvent(new StartFlightEvent(this));
            AeroportServer.TraceMessage("Рейс запущен: " + this);
        }


        private bool departureMessage;

        public virtual void Tick(long timeDelta)
        {
            if (!started || finished) return;

            long time = AeroportServer.Instance.TimeCounter;
            if (time >= DepartureTime)
            {
                if (!departureMessage)
                {
                    SetState(FlightState.FLYING);
                    AeroportServer.TraceMessage("Рейс взлетел: " + this);
                    departureMessage = true;
                }

                if (time > DeadlineDate && !ForfeitPayed)
                {
                    PayForfeit();
                }

                Airplane.CurrentCity = SrcCity;
                Progress = (time - DepartureTime) / (double) (ArrivalTime - DepartureTime);
                Airplane.CurrentPosition = SrcCity.Position + (DestCity.Position - SrcCity.Position) * Progress;

                EventHandlerRegistry.SendEvent(new FlightProgressEvent(this, Progress));

                if (time >= ArrivalTime)
                {
                    Airplane.CurrentCity = DestCity;
                    Airplane.CurrentPosition = DestCity.Position;
                    Progress = 1D;
                    finished = true;
                    Airplane.UsedInFlight = null;
                    BindedPlayer?.AddMoney(TransferPrice, "Выполненый рейс: " + this);

                    UnBind();

                    AeroportServer.TraceMessage("Рейс прибыл: " + this);
                    EventHandlerRegistry.SendEvent(new FinishFlightEvent(this));
                }

                EventHandlerRegistry.SendEvent(new AirplaneMoveEvent(Airplane));
            }
        }

        public override string ToString()
        {
            return "Рейс №" + Uuid + ". Вылет: " + SrcCity.ToString() + " в " + AeroportServer.Instance.TimeToDateTime(DepartureTime) + ". Прибытие: " + DestCity.ToString() + " в " + AeroportServer.Instance.TimeToDateTime(ArrivalTime);
        }

        public IList<IFlightCondition> CheckConditions()
        {
            return Conditions.FindAll(cond => !cond.Accept(this));
        }

        public bool CanStart()
        {
            return CheckConditions().Count == 0;
        }
    }

    class ForwardReverceFlight : DirectFlight
    {
        public ForwardReverceFlight(ICity srcCity, long endDate, ICity destCity, double price, int repeatDayTimes) : base(srcCity, endDate, destCity, price)
        {
            Regularity = FlightRegularity.FORWARD_REVERSE;
            RepeatDayTimes = repeatDayTimes;
        }

        public override void Tick(long timeDelta)
        {
            var p = BindedPlayer;

            base.Tick(timeDelta);

            if (Finished)
            {
                var frFlight = new ForwardReverceFlight(SrcCity, DeadlineDate + RepeatDayTimes * TimeHelper.SECONDS_IN_DAY, DestCity, TransferPrice, RepeatDayTimes)
                {
                    FlightType = FlightType,
                    ForfeitPrice = ForfeitPrice,
                    Regularity = Regularity
                };

                var reverceFlight = new DirectFlight(DestCity, DeadlineDate + RepeatDayTimes * TimeHelper.SECONDS_IN_DAY, SrcCity, 0);

                var coffeBreakTime = 60 * 90; // 90 минут пъем кофе

                reverceFlight.BindToPlayer(p, Airplane, ArrivalTime + coffeBreakTime);
                frFlight.BindToPlayer(p, Airplane, frFlight.DeadlineDate - frFlight.ComputeFlyingTimeForAirplane(Airplane) - coffeBreakTime);

                AeroportServer.Instance.World.AddFlight(reverceFlight);
                AeroportServer.Instance.World.AddFlight(frFlight);
            }
        }
    }
}
