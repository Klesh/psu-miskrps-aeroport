﻿using System;
using AeroportSimulator.Server.Api;

namespace AeroportSimulator.Server.Common
{
    class AirplaneType : IAirplaneType
    {
        public string Name { get; internal set; }
        public double Price { get; internal set; }
        public double Speed { get; internal set; }
        public double ServicePrice { get; internal set; }
        public double MaxFlyDistance { get; internal set; }
        public int PlaceCount { get; internal set; }
        public int MaxLiftingCapacity { get; internal set; }

        public AirplaneType(string name, double price)
        {
            Name = name;
            Price = price;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}