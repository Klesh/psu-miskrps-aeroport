﻿using AeroportSimulator.Server.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Common.Factory
{
    /// <summary>
    /// Фабрика для создания сервера
    /// </summary>
    public static class AeroportServerFactory
    {
        /// <summary>
        /// Создает сервер из дома и начальных денег
        /// </summary>
        /// <param name="home">Дом игрока</param>
        /// <param name="initMoney">Начальные деньги</param>
        /// <returns>Созданый экземпляр сервера</returns>
        public static ISimulationServer Create(ICity home, double initMoney)
        {
            ServerParams p = new ServerParams
            {
                Home = home,
                InitialMoney = initMoney
            };

            return Create(p);
        }

        /// <summary>
        /// Создает сервер с использованием пользовательских параметров
        /// </summary>
        /// <param name="p">Параметры сервера</param>
        /// <returns>Созданый экземпляр сервера</returns>
        public static ISimulationServer Create(ServerParams p)
        {
            var player = new Player(p.Home);
            player.AddMoney(p.InitialMoney, "Начальный баланс");
            return new AeroportServer(player, p);
        }
    }
}
