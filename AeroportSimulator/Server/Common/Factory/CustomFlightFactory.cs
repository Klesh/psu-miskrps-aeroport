﻿using AeroportSimulator.Server.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Common.Factory
{
    public static class CustomFlightFactory
    {
        public static void CreateCustomFlight(ICity srcCity, long endDate, ICity destCity, int price)
        {
            DirectFlight f = new DirectFlight(srcCity, endDate, destCity, price);
            AeroportServer.Instance.World.AddFlight(f);
        }
    }
}
