﻿using AeroportSimulator.Server.Api;
using AeroportSimulator.Server.Api.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace AeroportSimulator.Server
{
    class HandlerData
    {
        public Object holder;
        public MethodInfo mInfo;
        public SynchronizationContext uiContext;

        public HandlerData(Object holder, MethodInfo mInfo, SynchronizationContext uiContext)
        {
            this.holder = holder;
            this.mInfo = mInfo;
            this.uiContext = uiContext;
        }
    }

    public class EventHandlerRegistry
    {
        private static readonly EventHandlerRegistry INSTANCE = new EventHandlerRegistry();

        private Dictionary<Type, List<HandlerData>> handlerMap = new Dictionary<Type, List<HandlerData>>();

        private EventHandlerRegistry()
        {

        }

        public static void AddHadler(object h, SynchronizationContext uiContext)
        {
            foreach (var m in h.GetType().GetMethods())
            {
                foreach (var attr in m.GetCustomAttributes<EventHandlerAttribute>())
                {
                    INSTANCE.registerMethod(h, m, uiContext);
                }
            }
        }

        public static void RemoveHadler(object h)
        {
            foreach (var m in h.GetType().GetMethods())
            {
                foreach (var attr in m.GetCustomAttributes<EventHandlerAttribute>())
                {
                    INSTANCE.unregisterMethod(h, m);
                }
            }
        }

        public static void SendEvent(IEvent e)
        {
            List<HandlerData> handlers;
            if (INSTANCE.handlerMap.TryGetValue(e.GetType(), out handlers))
            {
                handlers.ForEach(h => h.uiContext.Post((_ => {
                    try
                    {
                        h.mInfo.Invoke(h.holder, new[] { e });
                    } catch (Exception ex)
                    {
                        throw new Exception("Ошибка при обработке события сервера: " + ex.ToString(), ex);
                    }
                }), null));
            }
        }

        private void registerMethod(object h, MethodInfo m, SynchronizationContext uiContext)
        {
            var ps = m.GetParameters();
            if (ps.Length == 0)
            {
                throw new Exception("Обработчик события не может быть без параметров: " + h.GetType().ToString() + "." + m.Name);
            }

            var eventType = m.GetParameters().First().ParameterType;
            if (!typeof(IEvent).IsAssignableFrom(eventType))
            {
                throw new Exception("Неправильный тип параметра у метода: " + h.GetType().ToString() + "." + m.Name + ". Параметр должен реализовывать " + typeof(IEvent).Name);
            }

            List<HandlerData> handlers;
            if (!handlerMap.TryGetValue(eventType, out handlers))
            {
                handlers = new List<HandlerData>();
                handlerMap.Add(eventType, handlers);
            }

            handlers.Add(new HandlerData(h, m, uiContext));
        }

        private void unregisterMethod(object h, MethodInfo m)
        {
            var ps = m.GetParameters();
            if (ps.Length == 0)
            {
                return;
            }

            var eventType = m.GetParameters().First().ParameterType;
            List<HandlerData> handlers;
            if (handlerMap.TryGetValue(eventType, out handlers))
            {
                handlers.RemoveAll((handler) => handler.holder == h);
            }
        }
    }
}
