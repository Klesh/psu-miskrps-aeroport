﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Api
{
    /// <summary>
    /// Тип рейса
    /// </summary>
    public enum FlightType
    {
        /// <summary>
        /// Пассажирский
        /// </summary>
        PASSENGER,

        /// <summary>
        /// Грузовой
        /// </summary>
        CARGO
    }

    /// <summary>
    /// Регулярность рейса
    /// </summary>
    public enum FlightRegularity
    {
        /// <summary>
        /// Только туда
        /// </summary>
        DIRECT,

        /// <summary>
        /// Туда и обратно
        /// </summary>
        FORWARD_REVERSE
    }

    /// <summary>
    /// Текущее направление рейса
    /// </summary>
    public enum FlightState
    {
        BIND_WAIT, PLANING, DEPARTURE_WAIT, FLYING, 
    }

    /// <summary>
    /// Сущность - рейс
    /// </summary>
    public interface IFlight : IIdEntity
    {
        /// <summary>
        /// Игрок, который взял рейс
        /// </summary>
        IPlayer BindedPlayer { get; }

        /// <summary>
        /// Самолет который поставил игрок
        /// </summary>
        IAirplane Airplane { get; }

        /// <summary>
        /// Тип рейса
        /// </summary>
        FlightType FlightType { get; }

        /// <summary>
        /// Регулярность рейса
        /// </summary>
        FlightRegularity Regularity { get; }

        /// <summary>
        /// Текущее направление рейса
        /// </summary>
        FlightState State { get; }

        /// <summary>
        /// Дата (день) Просрочки рейса в секундах
        /// </summary>
        long DeadlineDate { get; }

        /// <summary>
        /// Время прибытия рейса в секундах.
        /// Расчитывается после забора игроком и присвоения самолета (<see cref="BindToPlayer(IPlayer, IAirplane, long)"/>)
        /// Берется расстояние между городами и делится на скорость самолета 
        /// и к этому прибавляется время начала рейса (<see cref="DepartureTime"/>)
        /// </summary>
        long ArrivalTime { get; }

        /// <summary>
        /// Время вылета рейса в секундах.
        /// Расчитывается после забора игроком и присвоения самолета (<see cref="BindToPlayer(IPlayer, IAirplane, long)"/>)
        /// </summary>
        long DepartureTime { get; }

        /// <summary>
        /// Город назначения
        /// </summary>
        ICity DestCity { get; }

        /// <summary>
        /// Город вылета
        /// </summary>
        ICity SrcCity { get; }

        /// <summary>
        /// Неустойка, цена за взятый, но не выполненый рейс
        /// </summary>
        double ForfeitPrice { get; }

        /// <summary>
        /// Фактическая стоимость рейса, после присвоения самолета.
        /// Вычисляется для <see cref="FlightType.PASSENGER"/> исходя из стоимости билета * кол-во пассажиров + базовая цена.
        /// Вычисляется для<see cref= "FlightType.CARGO" /> исходя из фиксированной стоимости грузового рейса.
        /// </summary>
        double TransferPrice { get; }

        /// <summary>
        /// Флаг, показывающий, что рейс никто еще не взял
        /// </summary>
        bool Free { get; }

        /// <summary>
        /// Флаг, показывающий, что рейс просрочен
        /// </summary>
        bool Overdue { get; }

        /// <summary>
        /// Флаг, показывающий, что рейс выполнен
        /// </summary>
        bool Finished { get; }

        /// <summary>
        /// Прогресс рейса, расчитывается каждый тик, после наступления
        /// времени вылета.
        /// Принимает значения от 0 до 1 (типо 0% и 100%)
        /// </summary>
        double Progress { get; }

        /// <summary>
        /// Список условий, необходимых для начала выполнения рейса
        /// </summary>
        IReadOnlyList<IFlightCondition> Conditions { get; }

        /// <summary>
        /// Для регулярных рейсов. Интервал повторения в днях
        /// </summary>
        int RepeatDayTimes { get; }

        /// <summary>
        /// Метод позволяющий получить какие условия рейса НЕ выполняются
        /// </summary>
        /// <returns>Список не выполненых условий</returns>
        IList<IFlightCondition> CheckConditions();

        /// <summary>
        /// Позволяет игроку взять данный рейс, и выбрать для него самолет и время вылета
        /// </summary>
        /// <param name="p">Игрок</param>
        /// <param name="plane">Самолет игрока</param>
        /// <param name="localStartTime">Время начала полета в секундах относительно <see cref="StartDate"/></param>
        void BindToPlayer(IPlayer p, IAirplane plane, long localStartTime);

        /// <summary>
        /// Заменяет установленный самолет, на другой, если рейс уже выполняется
        /// то вернет false
        /// </summary>
        /// <param name="newPlane">Самолет для замены</param>
        bool ReplaceAirplane(IAirplane newPlane);

        /// <summary>
        /// Отказаться от рейса и заплатить неустойку
        /// </summary>
        void UnBind();

        /// <summary>
        /// Выполняет проверки необходимые для начала рейса
        /// </summary>
        /// <returns>Если все проверки и условия выполнены, то <see cref="true"/></returns>
        bool CanStart();

        /// <summary>
        /// Запускает рейс (Ставит на выполнение)
        /// Рейс начнет сразу же тикаться сервером
        /// </summary>
        void Start();
    }
}
