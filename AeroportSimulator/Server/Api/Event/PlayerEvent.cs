﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Api.Event
{
    /// <summary>
    /// Базовый класс для событий с игроком
    /// </summary>
    public abstract class PlayerEvent : IEvent
    {
        public IPlayer Player { get; set; }

        public PlayerEvent(IPlayer player)
        {
            Player = player;
        }
    }

    /// <summary>
    /// Событие, отправляемое при изменении полей игрока
    /// </summary>
    public class PlayerMoneyChangeEvent : PlayerEvent
    {
        public PlayerMoneyChangeEvent(IPlayer player) : base(player)
        {
        }
    }

    /// <summary>
    /// Базовый класс для событий игрока связанных с самолетами
    /// </summary>
    public abstract class PlayerAirplaneEvent : PlayerEvent
    {
        public IAirplane Airplane { get; set; }

        public PlayerAirplaneEvent(IPlayer player, IAirplane airplane) : base(player)
        {
            Airplane = airplane;
        }
    }

    /// <summary>
    /// Событие когда игроку дается самолет
    /// </summary>
    public class PlayerAirplaneAddingEvent : PlayerAirplaneEvent
    {
        public PlayerAirplaneAddingEvent(IPlayer player, IAirplane airplane) : base(player, airplane)
        {
        }
    }

    /// <summary>
    /// Событие когда у игрока забирается самолет
    /// </summary>
    public class PlayerAirplaneRemovingEvent : PlayerAirplaneEvent
    {
        public PlayerAirplaneRemovingEvent(IPlayer player, IAirplane airplane) : base(player, airplane)
        {
        }
    }
}
