﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Api.Event
{
    /// <summary>
    /// Событие при пройгрыше, закончились деньги и тд.
    /// </summary>
    public class GameOverEvent : IEvent
    {
        /// <summary>
        /// Причина завершения игры
        /// </summary>
        public string Reason { get; set; }

        public GameOverEvent(string reason)
        {
            Reason = reason;
        }
    }
}
