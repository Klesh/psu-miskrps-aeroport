﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Api.Event
{
    /// <summary>
    /// Событие для отображения различных внутренних событий сервера
    /// </summary>
    public class ServerTraceMessageEvent : IEvent
    {
        /// <summary>
        /// Описание события
        /// </summary>
        public string Message { get; set; }

        public ServerTraceMessageEvent(string message)
        {
            Message = message;
        }
    }
}
