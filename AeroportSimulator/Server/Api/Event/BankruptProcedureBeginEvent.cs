﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Api.Event
{
    /// <summary>
    /// Событие о начале процедуры банкротства
    /// </summary>
    public class BankruptProcedureBeginEvent : IEvent
    {
        private CountdownEvent countDownLatch = new CountdownEvent(1);

        /// <summary>
        /// Завершение процедуры, должен вызвать клиент
        /// </summary>
        public void Finish()
        {
            countDownLatch.Signal();
        }

        /// <summary>
        /// Блокирует текущий поток, до вызова <see cref="Finish()"/>.
        /// Вызывает сервер
        /// </summary>
        public void WaitForFinish()
        {
            countDownLatch.Wait();
        }
    }
}
