﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Api.Event
{
    /// <summary>
    /// Интерфес, реализуемый любым событием
    /// </summary>
    public interface IEvent
    {
    }
}
