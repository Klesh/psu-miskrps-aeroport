﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Api.Event
{
    /// <summary>
    /// Событие при перемещении самолета
    /// </summary>
    public class AirplaneMoveEvent : IEvent
    {
        public IAirplane Airplane { get; set; }

        public AirplaneMoveEvent(IAirplane Airplane)
        {
            this.Airplane = Airplane;
        }
    }
}
