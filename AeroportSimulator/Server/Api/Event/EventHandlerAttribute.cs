﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Api.Event
{
    /// <summary>
    /// Атрибут для указания <see cref="EventHandlerRegistry"/>, 
    /// что метод является обработчиком событий.
    /// Метод должен иметь следующую структуру: void Name({$param} e),
    /// где {$param} - тип, унаследованный от <see cref="IEvent"/>
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class EventHandlerAttribute : System.Attribute
    {
    }
}
