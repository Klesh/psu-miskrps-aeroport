﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Api.Event
{
    /// <summary>
    /// Событие при обновлении ассортимента магазина
    /// </summary>
    public class TraiderShowcaseUpdateEvent : IEvent
    {
        public ReadOnlyCollection<ILeasingTerms> LeasingTerms { get; set; }
        public ReadOnlyCollection<IRentTerms> RentTerms { get; set; }
        public ReadOnlyCollection<IAirplaneType> AirplaneShowcase { get; set; }

        public TraiderShowcaseUpdateEvent(IList<ILeasingTerms> leasing, IList<IRentTerms> rent, IList<IAirplaneType> arTypes)
        {
            LeasingTerms = new ReadOnlyCollection<ILeasingTerms>(leasing);
            RentTerms = new ReadOnlyCollection<IRentTerms>(rent);
            AirplaneShowcase = new ReadOnlyCollection<IAirplaneType>(arTypes);
        }
    }
}
