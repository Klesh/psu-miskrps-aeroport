﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Api.Event
{
    /// <summary>
    /// Базовый класс для всех событий связанных с самолетами
    /// </summary>
    public abstract class FlightEvent : IEvent
    {
        public IFlight Flight { get; set; }

        public FlightEvent(IFlight flight)
        {
            Flight = flight;
        }
    }

    /// <summary>
    /// Событие при генерации нового рейса
    /// </summary>
    public class NewGeneratedFlightEvent : FlightEvent
    {
        public NewGeneratedFlightEvent(IFlight flight) : base(flight)
        {
        }
    }

    /// <summary>
    /// Событие удалении рейса из мира, 
    /// например при успешном завершении, просрочке 
    /// или просто если никто его не взял
    /// </summary>
    public class WorldRemoveFlightEvent : FlightEvent
    {
        public WorldRemoveFlightEvent(IFlight flight) : base(flight)
        {
        }
    }

    /// <summary>
    /// Событие при добавлении рейса в мир, например генерация нового
    /// </summary>
    public class WorldAddFlightEvent : FlightEvent
    {
        public WorldAddFlightEvent(IFlight flight) : base(flight)
        {
        }
    }

    /// <summary>
    /// Событие при присоединении (забирании) рейса игроком себе
    /// </summary>
    public class BindFlightEvent : FlightEvent
    {
        public BindFlightEvent(IFlight flight) : base(flight)
        {
        }
    }

    /// <summary>
    /// Событие когда игрок отказывается от взятого рейса
    /// </summary>
    public class UnBindFlightEvent : FlightEvent
    {
        public UnBindFlightEvent(IFlight flight) : base(flight)
        {
        }
    }

    /// <summary>
    /// Событие при начале выполнении рейса
    /// </summary>
    public class StartFlightEvent : FlightEvent
    {
        public StartFlightEvent(IFlight flight) : base(flight)
        {
        }
    }

    /// <summary>
    /// Событие при завершении выполнения рейса
    /// </summary>
    public class FinishFlightEvent : FlightEvent
    {
        public FinishFlightEvent(IFlight flight) : base(flight)
        {
        }
    }

    /// <summary>
    /// Событие когда рейс просрачивается, истекает срок реализации.
    /// 
    /// <code>
    /// IFlight.DeadlineTime > AeroportServer.Instance.TimeCounter
    /// </code>
    /// 
    /// </summary>
    public class OverdueFlightEvent : FlightEvent
    {
        public OverdueFlightEvent(IFlight flight) : base(flight)
        {
        }
    }

    /// <summary>
    /// Событи при изменении статуса рейса
    /// </summary>
    public class FlightStatusChangeEvent : FlightEvent
    {
        public FlightState State { get; set; }

        public FlightStatusChangeEvent(IFlight flight) : base(flight)
        {
            State = flight.State;
        }
    }

    /// <summary>
    /// Событие для отображения прогресс бара для рейса
    /// </summary>
    public class FlightProgressEvent : FlightEvent
    {
        public double Progress { get; set; }

        public FlightProgressEvent(IFlight flight, double progress) : base(flight)
        {
            Progress = flight.Progress;
        }
    }
}
