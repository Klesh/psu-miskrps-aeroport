﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Api.Event
{
    /// <summary>
    /// Событие при оплате договора лизинга
    /// </summary>
    public class TraiderLeasingContractPaymentEvent : IEvent
    {
        public ILeasingContract Contract { get; set; }

        public TraiderLeasingContractPaymentEvent(ILeasingContract c)
        {
            Contract = c;
        }
    }

    /// <summary>
    /// Событие при оплате договора аренды
    /// </summary>
    public class TraiderRentContractPaymentEvent : IEvent
    {
        public IRentContract Contract { get; set; }

        public TraiderRentContractPaymentEvent(IRentContract c)
        {
            Contract = c;
        }
    }
}
