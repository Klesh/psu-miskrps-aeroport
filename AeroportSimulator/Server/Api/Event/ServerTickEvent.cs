﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Api.Event
{
    /// <summary>
    /// Событие очередного тика сервера, 
    /// отправляется после всех обработок в конце тика
    /// </summary>
    public class ServerTickEvent : IEvent
    {
        public long Time { get; set; }
        public long DeltaTime { get; set; }

        public ServerTickEvent(long time, long delta)
        {
            Time = time;
            DeltaTime = delta;
        }
    }
}
