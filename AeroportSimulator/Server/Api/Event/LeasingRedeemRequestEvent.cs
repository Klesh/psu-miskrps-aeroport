﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Api.Event
{
    /// <summary>
    /// Событие - запрос, возникает когда истекает срок лизинга самолета
    /// и нужно узнать у игрока, хочет он его выкупить или нет. При условии, 
    /// что у него достаточно денег, если их не хватает, то событие не будет 
    /// вызвано
    /// </summary>
    public class LeasingRedeemRequestEvent : IEvent
    {
        /// <summary>
        /// Самолет для выкупа
        /// </summary>
        public IAirplane Airplane { get; private set; }

        private CountdownEvent countDownLatch = new CountdownEvent(1);

        /// <summary>
        /// Остаточная цена, по которой выкупается самолет
        /// </summary>
        public double ResidualPrice { get; private set; }

        public LeasingRedeemRequestEvent(IAirplane airplane, double residualPrice)
        {
            Airplane = airplane;
            ResidualPrice = residualPrice;
        }

        /// <summary>
        /// Ответ игрока, нужно если игрок согласен, то надо записать сюда <see cref="true"/>
        /// </summary>
        public bool Agree { get; set; } = false;


        /// <summary>
        /// Завершение процедуры, должен вызвать клиент
        /// </summary>
        public void Finish()
        {
            countDownLatch.Signal();
        }

        /// <summary>
        /// Блокирует текущий поток, до вызова <see cref="Finish()"/>.
        /// Вызывает сервер
        /// </summary>
        public void WaitForFinish()
        {
            countDownLatch.Wait();
        }
    }
}
