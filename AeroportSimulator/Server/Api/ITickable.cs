﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Api
{
    /// <summary>
    /// Интерфес для сущностей, поддерживающих тики сервера
    /// </summary>
    public interface ITickable
    {
        /// <summary>
        /// Вызывается сервером при тике сущности
        /// </summary>
        /// <param name="timeDelta">Время в секундах, прошедшее с предыдущего тика</param>
        void Tick(long timeDelta);
    }
}
