﻿using AeroportSimulator.Server.Api.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Api
{
    /// <summary>
    /// Сущность - город
    /// </summary>
    public interface ICity : IIdEntity
    {
        /// <summary>
        /// Название города
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Позиция в мире
        /// </summary>
        Vector2D Position { get; }
    }
}
