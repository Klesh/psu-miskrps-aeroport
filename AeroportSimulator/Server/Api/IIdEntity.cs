﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Api
{
    /// <summary>
    /// Интерфес для всех сущностей. Содержит уникальный номер (uuid)
    /// </summary>
    public interface IIdEntity
    {
        /// <summary>
        /// Уникальный номер сущности
        /// </summary>
        long Uuid { get; }
    }
}
