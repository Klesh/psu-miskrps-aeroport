﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Api
{
    /// <summary>
    /// Мир
    /// </summary>
    public interface IWorld
    {
        /// <summary>
        /// Список всех рейсов, занятых, выполняющихся и свободные
        /// </summary>
        IReadOnlyList<IFlight> AllFlights { get; }

        /// <summary>
        /// Текущий игрок в мире
        /// </summary>
        IPlayer Player { get; }
    }
}
