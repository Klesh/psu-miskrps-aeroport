﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Api
{
    /// <summary>
    /// Сущность - Игрок
    /// </summary>
    public interface IPlayer : IIdEntity
    {
        /// <summary>
        /// Текущий баланс игрока
        /// </summary>
        double Money { get; }

        /// <summary>
        /// Дом игрока
        /// </summary>
        ICity Home { get; }

        /// <summary>
        /// Список самолетов игрока, купленных, арендованных или взятых в лизинг
        /// </summary>
        IReadOnlyList<IAirplane> Airplanes { get; }

        /// <summary>
        /// Метод для проверки достаточночти денег
        /// </summary>
        /// <param name="amount">Сколько денег нужно</param>
        /// <returns><see cref="true"/> если баланс игрока выше требуемой суммы</returns>
        bool HasMoney(double amount);
    }
}
