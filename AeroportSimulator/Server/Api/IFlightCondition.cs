﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Api
{
    /// <summary>
    /// Условие для успешного начала и выполнения рейса
    /// </summary>
    public interface IFlightCondition
    {
        /// <summary>
        /// Вызывается для осущетсвления проверки
        /// </summary>
        /// <param name="flight">Рейс</param>
        /// <returns><see cref="true"/> если проверка прошла</returns>
        bool Accept(IFlight flight);
    }
}
