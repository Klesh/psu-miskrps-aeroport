﻿using AeroportSimulator.Server.Common.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Api
{
    /// <summary>
    /// Текущее состояние о форме собственности
    /// </summary>
    public enum OwnershipState
    {
        /// <summary>
        /// Взят в лизинг
        /// </summary>
        IN_LEASING,

        /// <summary>
        /// Взят в аренду
        /// </summary>
        IN_RENT,

        /// <summary>
        /// Выкуплен или куплен
        /// </summary>
        PURCHAESED
    }

    /// <summary>
    /// Договор между игроком и трейдером
    /// </summary>
    public interface ITraiderContract
    {
        /// <summary>
        /// Игрок заключивший договор
        /// </summary>
        IPlayer Player { get; }

        /// <summary>
        /// Время окончания договора
        /// </summary>
        long EndTime { get; }

        /// <summary>
        /// Оставшееся время действия договора
        /// </summary>
        long ResidualTime { get; }

        /// <summary>
        /// Время следующей оплаты
        /// </summary>
        long NextPayTime { get; }

        /// <summary>
        /// Субьект договора (самолет)
        /// </summary>
        IAirplane Airplane { get; }

        /// <summary>
        /// Имя договора, Аренда, Лизинг...
        /// </summary>
        String Name { get; }
    }

    /// <summary>
    /// Договор лизинга
    /// </summary>
    public interface ILeasingContract : ITraiderContract
    {
        /// <summary>
        /// Условаия лизинга
        /// </summary>
        ILeasingTerms Terms { get; }
    }

    /// <summary>
    /// Договор аренды
    /// </summary>
    public interface IRentContract : ITraiderContract
    {
        /// <summary>
        /// условия аренды
        /// </summary>
        IRentTerms Terms { get; }
    }

    /// <summary>
    /// Предложения для аренды самолетов
    /// </summary>
    public interface IRentTerms
    {
        /// <summary>
        /// Тип арендуемого самолета
        /// </summary>
        IAirplaneType PlaneType { get; }

        /// <summary>
        /// Штраф за не выполнение договора. 
        /// За каждый день просрока возврата самолета будет уплачиваться штраф
        /// </summary>
        double MoneyPenalty { get; }

        /// <summary>
        /// Арендная цена за месяц с момента аренды
        /// </summary>
        double Price { get; }
    }

    /// <summary>
    /// Предложения для лизинга самолета, наследует <see cref="RentTerms"/>
    /// </summary>
    public interface ILeasingTerms : IRentTerms
    {
        /// <summary>
        /// Возможность выкупа самолета по остаточной цене
        /// </summary>
        bool Redeem { get; }

        /// <summary>
        /// Остаточная цена выкупа, если есть возможноть и хватает денег у игрока,
        /// то после окончания действия лизинга, со счета игрока спишется эта сумма,
        /// а самолет не будет изъят
        /// </summary>
        double ResidualPrice { get; }
    }


    /// <summary>
    /// Интерфейс магазина самолетов
    /// </summary>
    public interface IAirplaneTraider
    {
        /// <summary>
        /// Предложения по аренде самолетов
        /// </summary>
        /// <returns>Не изменяемый список предложений по аренде</returns>
        IReadOnlyList<IRentTerms> RentTerms();

        /// <summary>
        /// Предложения по лизингу самолетов
        /// </summary>
        /// <returns>Не изменяемый список предложений по лизингу</returns>
        IReadOnlyList<ILeasingTerms> LeasingTerms();

        /// <summary>
        /// Предложения по простой покупке самолетов за полную цену
        /// </summary>
        /// <returns>Не изменяемый список предложений по покупке</returns>
        IReadOnlyList<IAirplaneType> AirplaneShowcase();

        /// <summary>
        /// Взять самолет в лизинг. По истечению срока лизинга, самолет изымается у 
        /// игрока елси он отказался его выкупать по остаточной стоимости или у него нет средств
        /// </summary>
        /// <param name="p">Игрок который берет самолет в лизинг</param>
        /// <param name="terms">Условие лизинга, полученное из метода <see cref="LeasingTerms()"/></param>
        /// <param name="time">Время на которое оформляется лизинг в секундах</param>
        /// <returns>Вслуче успеха, взятый в лизинг самолет, иначе <see cref="null"/></returns>
        IAirplane LeaseAirplane(IPlayer p, ILeasingTerms terms, long time);

        /// <summary>
        /// Взять самолет в аренду. По истечению срока аренды, самолет изымается у игрока
        /// </summary>
        /// <param name="p">Игрок который берет самолет в аренду</param>
        /// <param name="terms">Условие аренды, полученное из метода <see cref="RentTerms()"/></param>
        /// <param name="time">Время на которое оформляется аренда в секундах</param>
        /// <returns>Вслуче успеха, взятый в аренду самолет, иначе <see cref="null"/></returns>
        IAirplane RentAirplane(IPlayer p, IRentTerms terms, long time);

        /// <summary>
        /// Купить самолет по полной стоимости
        /// </summary>
        /// <param name="p">Игрок который покупает самолет</param>
        /// <param name="type">Тип самолета для покупки </param>
        /// <seealso cref="AirplaneTypes"/>
        /// <returns>В случае успеха, купленный самолет, иначе <see cref="null"/></returns>
        IAirplane BuyAirplane(IPlayer p, IAirplaneType type);

        /// <summary>
        /// Продать самолет
        /// </summary>
        /// <param name="p">Игрок который продает самолет</param>
        /// <param name="plane">Самолет для продажи</param>
        /// <returns>В случае успеха, <see cref="true"/></returns>
        bool SellAirplane(IPlayer p, IAirplane plane);


        /// <summary>
        /// Получить статус формы собственноести на игрока и самолет
        /// </summary>
        /// <param name="p">Игрок</param>
        /// <param name="plane">Самолет</param>
        /// <returns>Статус формы собственности</returns>
        OwnershipState OwnershipState(IPlayer p, IAirplane plane);

        /// <summary>
        /// Получить договор на лизинг
        /// </summary>
        /// <param name="p">Игрок</param>
        /// <param name="plane">Самолет</param>
        /// <returns>Договор на лизинг или <see cref="null"/> если самолет куплен или выкуплен</returns>
        ILeasingContract LeasingСontract(IPlayer p, IAirplane plane);

        /// <summary>
        /// Получить договор на аренду
        /// </summary>
        /// <param name="p">Игрок</param>
        /// <param name="plane">Самолет</param>
        /// <returns>Договор на аренду или <see cref="null"/> если самолет куплен или выкуплен</returns>
        IRentContract RentСontract(IPlayer p, IAirplane plane);
    }
}
