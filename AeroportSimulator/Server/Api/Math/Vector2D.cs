﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Api.Math
{
    /// <summary>
    /// Класс для работы с векторами, нужен для обработки рейсов
    /// </summary>
    public class Vector2D
    {
        public static readonly Vector2D ZERO = new Vector2D(0, 0);

        public double X { get; set; }
        public double Y { get; set; }

        public Vector2D()
        {

        }

        public Vector2D(double x, double y)
        {
            X = x; Y = y;
        }

        public double Distance(Vector2D v)
        {
            return Vector2D.Distance(this, v);
        }

        public double Lenght()
        {
            return Vector2D.Lenght(this);
        }

        public Vector2D Abs()
        {
            return new Vector2D(System.Math.Abs(X), System.Math.Abs(Y));
        }

        public Vector2D Normalize()
        {
            var l = Lenght();
            if (l == 0) return ZERO;

            return new Vector2D(X / l, Y / l);
        }

        public Vector2D Negate()
        {
            return new Vector2D(-X, -Y);
        }

        public static Vector2D operator +(Vector2D v1, Vector2D v2)
        {
            return new Vector2D(v1.X + v2.X, v1.Y + v2.Y);
        }

        public static Vector2D operator -(Vector2D v1, Vector2D v2)
        {
            return new Vector2D(v1.X - v2.X, v1.Y - v2.Y);
        }

        public static Vector2D operator *(Vector2D v1, Vector2D v2)
        {
            return new Vector2D(v1.X * v2.X, v1.Y * v2.Y);
        }

        public static Vector2D operator /(Vector2D v1, Vector2D v2)
        {
            return new Vector2D(v1.X / v2.X, v1.Y / v2.Y);
        }

        public static Vector2D operator +(Vector2D v1, double d)
        {
            return new Vector2D(v1.X + d, v1.Y + d);
        }

        public static Vector2D operator -(Vector2D v1, double d)
        {
            return new Vector2D(v1.X - d, v1.Y - d);
        }

        public static Vector2D operator *(Vector2D v1, double d)
        {
            return new Vector2D(v1.X * d, v1.Y * d);
        }

        public static Vector2D operator /(Vector2D v1, double d)
        {
            return new Vector2D(v1.X / d, v1.Y / d);
        }

        public static double Distance(Vector2D v1, Vector2D v2)
        {
            return (v1 - v2).Lenght();
        }

        public static double Lenght(Vector2D v)
        {
            return System.Math.Sqrt(v.X * v.X + v.Y * v.Y);
        }
    }
}
