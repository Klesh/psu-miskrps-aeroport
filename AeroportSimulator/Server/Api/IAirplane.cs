﻿using AeroportSimulator.Server.Api.Math;
using AeroportSimulator.Server.Common;
using AeroportSimulator.Server.Common.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Api
{
    /// <summary>
    /// Тип самолета, содержит информацию о параметрах самолета
    /// </summary>
    public interface IAirplaneType
    {
        /// <summary>
        /// Цена самолета в магазине
        /// </summary>
        double Price { get; }

        /// <summary>
        /// Название самолета
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Скорость самолета в км/ч
        /// </summary>
        double Speed { get; }

        /// <summary>
        /// Цена обслуживания самолета
        /// </summary>
        double ServicePrice { get; }

        /// <summary>
        /// Максимальная длинна перелета
        /// </summary>
        double MaxFlyDistance { get; }

        /// <summary>
        /// Количество мест всего
        /// </summary>
        int PlaceCount { get; }

        /// <summary>
        /// Максимальная грузоподъемность
        /// </summary>
        int MaxLiftingCapacity { get; }
    }

    /// <summary>
    /// Сущность - самолет
    /// </summary>
    public interface IAirplane : IIdEntity
    {
        /// <summary>
        /// Тип самолета
        /// </summary>
        IAirplaneType Type { get; }

        /// <summary>
        /// Флаг определяющий, нуждается ли самолет в обслуживании
        /// </summary>
        bool NeedService { get; }

        /// <summary>
        /// Флаг определящий, был ли самолет обслужен
        /// </summary>
        bool Serviced { get; }

        /// <summary>
        /// Вектор, Текущая позиция в мире (X и Y координаты)
        /// </summary>
        Vector2D CurrentPosition { get; }

        /// <summary>
        /// Вектор, Предыдущая позиция в мире (X и Y координаты)
        /// </summary>
        Vector2D PrevPosition { get; }

        /// <summary>
        /// Текущий город в котором находится самолет
        /// </summary>
        ICity CurrentCity { get; }

        /// <summary>
        /// Рейс в котором участвует самолет в текущий момент
        /// </summary>
        IFlight UsedInFlight { get; }

        /// <summary>
        /// Количество занятых мест
        /// </summary>
        int BusyPlacesCount { get; }

        /// <summary>
        /// Текущий вес груза
        /// </summary>
        int CurrentWeight { get; }
    }
}
