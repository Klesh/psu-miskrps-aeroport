﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AeroportSimulator.Server.Api
{
    /// <summary>
    /// Интерфейс локального сервера игры
    /// </summary>
    public interface ISimulationServer
    {
        /// <summary>
        /// Текущий экземпляр магазина
        /// </summary>
        IAirplaneTraider Traider { get; }

        /// <summary>
        /// Текущее время на сервере. Увеличивается с каждым тиком
        /// </summary>
        long TimeCounter { get; }

        /// <summary>
        /// Текущий игрок на сервере
        /// </summary>
        IPlayer Player { get; }

        /// <summary>
        /// Список свободных, никем не занятых рейсов
        /// </summary>
        IReadOnlyList<IFlight> FreeFlights { get; }

        /// <summary>
        /// Список рейсов, которые занял игрок
        /// </summary>
        IReadOnlyList<IFlight> PlayerLights { get; }

        /// <summary>
        /// Список рейсов, который находятся на стадии выполнения
        /// </summary>
        IReadOnlyList<IFlight> RunningFlights { get; }

        /// <summary>
        /// Экземпляр мира со всеми рейсами, самолетами и тд.
        /// </summary>
        IWorld World { get; }


        /// <summary>
        /// Устанавливает временной интервал одного тика сервереа.
        /// Сервер тикает <see cref="AeroportServer.TICKS_PER_SECOND"/> раз в секунду.
        /// По умолчанию 1 тик = 1 секунде игрового времени.
        /// </summary>
        /// <param name="interval">Временной интервал тика</param>
        void SetTickInterval(long interval);

        /// <summary>
        /// Метод для обслуживания самолета. Снимает со счета игрока средства
        /// в размере платы за обслуживание самолета.
        /// Если самолет уже обслужен, то ничего не будет сделано
        /// </summary>
        /// <param name="plane"></param>
        /// <returns><see cref="true"/> если хватает денег для обслуживания</returns>
        bool ServiceAirplane(IAirplane plane);

        /// <summary>
        /// Регистрирует обработчик событий от сервера.
        /// </summary>
        /// <param name="handler">Обьект с методами обработчиками</param>
        /// <param name="uiContext">Контекст синхронизации для клиента. <see cref="SynchronizationContext.Current"/></param>
        void RegisterEventHandler(object handler, SynchronizationContext uiContext);

        /// <summary>
        /// Описывает обработчика от получения событий с сервера
        /// </summary>
        /// <param name="handler">Обьект с методами обработчиками</param>
        void UnregisterEventHandler(object handler);

        /// <summary>
        /// Запуск сервера в отдельном потоке
        /// </summary>
        void Start();

        /// <summary>
        /// Остановка сервера и его потока выполнения
        /// </summary>
        void Stop();

        /// <summary>
        /// Метод для получения текущего времени на сервере в читабельном формате
        /// </summary>
        /// <returns>Время на сервере в читабельном формате</returns>
        DateTime CurrentDate();

        /// <summary>
        /// Осуществляет перевод времени в секундах в читабельный формат
        /// </summary>
        /// <param name="seconds"></param>
        /// <returns>Время в читабельном формате</returns>
        DateTime TimeToDateTime(long seconds);

        /// <summary>
        /// Осуществляет перевод времени в локальное время сервера в секундах
        /// </summary>
        /// <param name="seconds"></param>
        /// <returns>Время сервера в секундах</returns>
        long DateTimeToTime(DateTime dateTime);

        /// <summary>
        /// Задает начальное время на сервере в секундах, оно будет исспользовано
        /// в методе <see cref="CurrentDate()"/>
        /// </summary>
        /// <param name="seconds">Начальное время в секундах</param>
        void SetInitialTimeSeconds(long seconds);

        /// <summary>
        /// Возобновлляет сервер после паузы
        /// </summary>
        void Resume();

        /// <summary>
        /// Приостанавливает сервер и все процессы
        /// </summary>
        void Pause();
    }
}
